/*
 * Version: 1.2
 */
(function($) {
    $(document).on("ready",function() {
        $('#menu-search .facetwp-search').on('keypress', function(e) {
            if(e.which === 13){
		        term = this.value;
                loadSearch(term);
            }
        });
        $('#menu-search .facetwp-btn').on("click", function(e) {
            term = $(this).parent().find(".facetwp-search").val();
            loadSearch(term);
        });

        function loadSearch(term){
            //FWP.parse_facets();
            //FWP.set_hash();
            var prefix = (window.location.hostname === "localhost") ? "/asd-wp/" : "";
            window.location.href = prefix + "/threats/?fwp_keywords=" + term;
            //window.location.href = "/search-results/" + window.location.search;
        }
    });
    
})(jQuery);
