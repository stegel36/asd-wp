<?php

     function avada_asdfp_activation() {
         set_transient('asdfp-admin-notice-show', true, 100);
     }

     add_action('admin_notices', 'asdfp_admin_notice_run');

     function asdfp_admin_notice_run() {
         if(get_transient('asdfp-admin-notice-show')) {
            ?>
            <div class="notice notice-warning is-dismissible">
                <p>Add the new Fusion Builder elements in Fusion Builder->Settings ->Fusion Builder Elements (<strong>ASD Featured Post</strong>).</p>
            </div>
            <?php
            delete_transient('cptt-admin-notice-show');
         }
     }

     // deactivation
     function avada_asdfp_deactivation()
     {}

    register_activation_hook(plugin_dir_path(__FILE__) . 'asd-featured-post.php', 'avada_asdfp_activation');
    register_deactivation_hook(plugin_dir_path(__FILE__) . 'asd-featured-post.php', 'avada_asdfp_deactivation');
?>