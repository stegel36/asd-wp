<?php
/**
 * Fusion-Builder Shortcode Element for ASD Featured Post.
 *
 * @package ASD-Featurd-Post
 * @since 1.0
 */

/**
 * Shortcode class.
 *
 * @package ASD-Featured-Post
 * @since 1.0
 */

 if(!class_exists('FusionSC_Featured_Post') && class_exists('FusionCore_Plugin')) {
     class FusionSC_Featured_Post extends Fusion_Element
     {
        /**
         * The Limit on excerpt length
         *
         * @access private
         * @since 1.0
         * @var integer
        */
        private $excerpt_length;

        /**
         * The Unique Class for prefixing everything
         *
         * @access private
         * @since 1.0
         * @var string
        */
        private $unique_class;

        /**
         * An array of the shortcode arguments.
         *
         * @static
         * @access public
         * @since 1.0
         * @var array
         */
        public static $args;

        /**
         * Constructor.
         *
         * @access public
         * @since 1.0
         */
        public function __construct()
        {

            parent::__construct();
            add_action('fusion_featured_post_shortcode_content', array($this, 'get_post_content'));
            add_filter('fusion_featured_post-shortcode', array($this, 'attr'));
            add_action('wp_enqueue_scripts', array( $this, 'fusion_asdfp_scripts' ) );
            add_shortcode('fusion_asd_featured_post', array($this, 'render'));
        }

        function fusion_asdfp_scripts()
        {
            
            wp_enqueue_style('fusion_asdfp_styles', plugins_url('/css/featured-post.css', __FILE__), array(), '1.0');
        }
        /**
         * Render the shortcode
         *
         * @access public
         * @since 1.0
         * @param  array $args Shortcode parameters.
         * @param  string $content Content between shortcode.
         * @return string          HTML output.
         */
        public function render($args, $content = '')
        {
            global $fusion_settings, $fusion_library;
            
            $this->unique_class = 'asd-' . rand();
            $limit = 50;
            $html = "<div class='$this->unique_class'>";

            $defaults = apply_filters(
                'fusion_portfolio_default_parameter',
                FusionBuilder::set_shortcode_defaults(
                    array(
                        'post_count' => "10",
                        'cpt_post_type' => 'post',
                        'cus_taxonomy' => '',
                        'cus_terms' => '',
                        'excerpt' => 'no',
                        'excerpt_length' => '100',
                        'post_type' => "post"
                    ), $args
                )
                );
            
            //get taxonomy name - portfolio_category
            $taxonomy_raw = $defaults['cus_taxonomy'];
            // get taxonomy name separate from post type
            preg_match("/(.+)__(.+)/", $taxonomy_raw, $taxonomy_match_array);
            $taxonomy_name_real = $taxonomy_match_array[2];

            // get term name separate from taxonomy
            if (!empty($defaults['cus_terms'])) {
                $term_name_real = array();
                $taxonomy_terms_raw = explode(',', $defaults['cus_terms']);
                
                foreach ($taxonomy_terms_raw as $term_raw_item) {
                    preg_match("/(.+)__(.+)/", $term_raw_item, $term_match_array);
                    $term_name_real[] = $term_match_array[2];
                }
            }
            
            $args = array( 
                'post_type' => $defaults['post_type'],
                'posts_per_page' => $defaults['post_count'],
                'orderby' => 'date',
                'tax_query' => array(array(
                    'taxonomy'  => $taxonomy_name_real,
                    'field'     => 'slug',
                    'terms'     => $term_name_real
                ) ));
                

            $posts = get_posts($args);
            foreach($posts as $post){
                
                setup_postdata( $post );
                // Post Object Looks Like
                // WP_Post Object ( [ID] => 9 [post_author] => 1 [post_date] => 2018-02-27 22:11:15 [post_date_gmt] => 2018-02-28 03:11:15 [post_content] => 
                // [post_title] => Russia's Disinformation Offensive [post_excerpt] => [post_status] => publish [comment_status] => open [ping_status] => open 
                // [post_password] => [post_name] => russias-disinformation-offensive [to_ping] => [pinged] => [post_modified] => 2018-03-27 20:49:28 [post_modified_gmt] => 2018-03-28 01:49:28 
                // [post_content_filtered] => [post_parent] => 0 [guid] => http://localhost/asd-wp/?p=9 [menu_order] => 0 [post_type] => post [post_mime_type] => [comment_count] => 0 
                // [filter] => raw )
                
                // get post categories
                $categories = get_the_category($post->ID);

                // get the tags
                $tags = get_the_tags($post->ID);
                $tag_text = "";
                
                if($tags)
                {
                    foreach ($tags as $tag)
                    {
                        $tag_text .= $tag->name . ", ";
                    }
                }

                $tag_text = rtrim($tag_text,", ");

                // get the post featured image
                
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                // $featured_image = get_the_post_thumbnail($post->ID,"",array( 'class' => 'article__image' ));
                // $featured_image = get_the_post_thumbnail($post->ID,"full",array( 'class' => 'article__image' ));
                $featured_image = "<img src='$image[0]' class='article__image' />";
            
                // start the article container
                $html .= "<article class='asd-article asd-article--featured'>";
                $html .= $featured_image;
                $html .= "<div class='article__content'>";
                $html .= '<h4 class="article__type">' . $categories[0]->name . '</h4>';
                $html .= '<p class="article__tags">' . $tag_text . '</p>';
                $html .= '<h3 class="article__title"><a href="' . get_post_permalink($post->ID, true) . '">' . $post->post_title . '</a></h3>';
                $html .= '<p class="article__byline">By ';
                if(function_exists("get_coauthors")) {
                    $coauthors = get_coauthors($post->ID);
                    if(is_array($coauthors) ) {
                        
                        foreach($coauthors as $theAuthor) {
                            $html .= coauthors_posts_links_single($theAuthor) . " and ";
                        }

                        $html = rtrim($html," and ");
                    }
                }
                $html .= "</p>";
                
                // add excerpt
                if($defaults['excerpt'] === 'yes') {
                    $html .= '<div class="article__excerpt"><a href="' . get_post_permalink($post->ID, true) . '">';
                  

                    $html .= fusion_builder_get_post_content($post->ID, "yes", $defaults['excerpt_length'], true);


                    $html .= '</a></div>';
                }
               
                $html .= "</div></article>";
            // endwhile;endif;
            }

            $html .= "</div>";
            return $html;
        }

        /**
         * Echoes the post-content.
         *
         * @access public
         * @since 1.0
         * @return void
         */
        public function get_post_content() {

            if ( 'no_text' !== self::$args['content_length'] ) {
                $excerpt = 'no';
                if ( 'excerpt' === strtolower( self::$args['content_length'] ) ) {
                    $excerpt = 'yes';
                }

                echo fusion_get_post_content( '', $excerpt, self::$args['excerpt_length'], self::$args['strip_html'] ); // WPCS: XSS ok.
            }
        }

        /**
         * Builds the attributes array.
         *
         * @access public
         * @since 1.0
         * @return array
         */
        public function attr() {
            echo "attr";
        }
    }

    new FusionSC_Featured_Post();
 }

 /**
  * Map shortcode to Fusion Builder
  * @since 1.0
  */
  function map_asd_featured_post() {
    $post_types = AvadaASDFPHelper::get_post_types();
    $taxonomies = AvadaASDFPHelper::get_taxonomies_list();
    $custom_terms = AvadaASDFPHelper::get_custom_terms();

    // Map settings for parent shortcode
    fusion_builder_map(
        array(
            'name'          => esc_attr__( 'ASD Featured Post', 'fusion-builder'),
            'shortcode'     => 'fusion_asd_featured_post',
            'icon'          => 'fa fa-map',
            'preview'       =>  ASD_FEATURED_POST_PLUGIN_DIR . 'preview/asd-featured-post-preview.php',
            'preview_id'    => 'fusion-builder-block-module-asd-featured-post-template',
            'params'        => array(
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_attr__( 'Post Count', 'fusion-builder'),
                    'description'   => esc_attr__( 'Enter the number of posts to display', 'fusion-builder'),
                    'param_name'    => 'post_count',
                    'value'         => '1'
                ),
                array(
                    'type' => 'select',
                    'heading' => esc_attr__('Post type', 'fusion-builder'),
                    'description' => esc_attr__('Choose your custom post type or leave post for standard Blog.', 'fusion-builder'),
                    'param_name' => 'cpt_post_type',
                    'default' => 'post',
                    'value' => $post_types,
                ),
                array(
                    'type' => 'select',
                    'heading' => esc_attr__('Custom Taxonomy', 'fusion-builder'),
                    'description' => esc_attr__('Select a custom taxonomy if you want to filter by taxonomy terms.', 'fusion-builder'),
                    'param_name' => 'cus_taxonomy',
                    'default' => 'select_taxonomy',
                    'value' => $taxonomies,
                ),
                array(
                    'type' => 'multiple_select',
                    'heading' => esc_attr__('Custom Term', 'fusion-builder'),
                    'description' => esc_attr__('Select terms in selected custom taxonomy.', 'fusion-builder'),
                    'param_name' => 'cus_terms',
                    'value' => $custom_terms,
                    'default' => '',
                ),
                array(
					'type'        => 'radio_button_set',
					'heading'     => esc_attr__( 'Show Excerpt', 'fusion-builder' ),
					'description' => esc_attr__( 'Controls if the post excerpt is displayed.', 'fusion-builder' ),
					'param_name'  => 'excerpt',
					'value'   => array(
						'yes'   => esc_attr__( 'Show', 'fusion-builder' ),
						'no'    => esc_attr__( 'Hide', 'fusion-builder' )
					),
					'default' => 'no',
                ),
                array(
					'type'        => 'range',
					'heading'     => esc_attr__( 'Excerpt Length', 'fusion-builder' ),
					'description' => esc_attr__( 'Insert the number of words/characters you want to show in the excerpt.', 'fusion-builder' ),
					'param_name'  => 'excerpt_length',
					'min'         => '0',
					'max'         => '500',
                    'step'        => '1',
                    'value'       => '50',
					'dependency'  => array(
						array(
							'element'  => 'excerpt',
							'value'    => 'no',
							'operator' => '!=',
						)
					),
				),
            )  
        )
    );
  }

  add_action( 'wp_loaded', 'map_asd_featured_post', 11);