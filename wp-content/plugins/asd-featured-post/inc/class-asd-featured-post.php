<?php
/**
 * This plugin creates a FusionBuilder element for a featured post
 * @since 1.0
 * @package ASD Featured Post for Fusion Builder
 */

 /**
  * The main plugin class
  */
//   class ASD_Featured_Post extends Fusion_Element{
      class ASD_Featured_Post{
      /**
       * One instance of object
       * @static
       * @access private
       * @since 1.0
       * @var object
       */
      private static $instance;

      /**
       * Create or return instance of class
       * @static
       * @access public
       * @since 1.0
       */

       public static function get_instance() {
           // if not yer created and set to $instance create a new one
           if( null === self::$instance ) {
               self::$instance = new ASD_Featured_Post();
           } 

           return self::$instance;
       }

       /**
        * Constructor
        * @since 1.0        
        */

        public function __construct() {
            // parent::__construct();
            add_action('wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

            add_shortcode( 'fusion_asd_featured_post', array( $this, 'render' ) );

            // New Settings Field to Fusion Builder
            // add_filter( 'fusion_builder_fields', array ( $this, 'add_new_field'));
        }

        /** Enqueue Scripts and styles 
         * @access public
         * @since 1.0
        */
        public function enqueue_scripts() {
            wp_enqueue_style( 'asd-featured-post-css', plugins_url( 'css/featured-post.css', __FILE__ ) );
            
        }

        //returns custom post type, taxonomies and terms in one array
        public static function all_custom_data() {

            //Array to be returned by the function
            $all_post_types_taxonomy=array();

            // create array for post type options
            $conf_pt= array (
                'public' => true
            );

            // get registered custom post types
            $custom_post_types_default = array('post'=> 'post');
            $custom_post_types_registered = get_post_types( $conf_pt, 'names', 'and');
            $custom_post_types = array_merge($custom_post_types_default, $custom_post_types_registered);


            //assign taxonomies to posts
            $args_terms = array(
                'orderby'    => 'count',
                'order' => 'DESC',
                'hide_empty' => 0,
            );

            foreach ($custom_post_types as $custom_post_type) {

                //get the list of taxonomies for the post tyep
                $taxonomy_objects = get_object_taxonomies( $custom_post_type);

                //get the list of terms object for the taxonomy
                unset($custom_taxonomies);
                $custom_taxonomies = array();
                foreach ($taxonomy_objects as $taxonomy_object) {

                    $terms = get_terms($taxonomy_object, $args_terms);
                    //cretae the array of taxonomies
                    $custom_taxonomies[$taxonomy_object] = $terms;
                }

                //create the array of post types
                $all_post_types_taxonomy[$custom_post_type]= $custom_taxonomies;
            }
            return $all_post_types_taxonomy;
            //	return $custom_taxonomies;

        }

    

//	return custom terms
public static function get_custom_terms() {
$all_custom_array = self::all_custom_data();

$custom_terms_formatted=array();
foreach ($all_custom_array as $custom_post_type => $taxonomies){

    foreach ($taxonomies as $taxonomy => $terms) {

        foreach ( $terms as $term) {
            $custom_terms_formatted[$taxonomy.'__'.$term->slug] = esc_attr__($term->name.' ('.$term->count.')', 'fusion-core');
        }

    }

}
return $custom_terms_formatted;

}
        
        /**
         * Returns the content
         * 
         * @access public
         * @since 1.0
         * @param array $atts The attributes array
         * @param string $content The content.
         * @return string
         */
        public function render($atts, $content ) {
            $unique_class = 'asd-' . rand();
            $limit = 50;
            $html .= "<div class='$unique_class'>";

            // get taxonomy name separate from post type
            preg_match("/(.+)__(.+)/", $atts['cus_taxonomy'], $taxonomy_match_array);
            $taxonomy_name_real = $taxonomy_match_array[2];

            // $test = fusion_builder_get_post_content('', "yes", "100", "yes");
            // echo $test;

            // get term name separate from taxonomy
            if (!empty($atts['cus_terms'])) {
                $term_name_real = array();
                $taxonomy_terms_raw = explode(',', $atts['cus_terms']);
                
                foreach ($taxonomy_terms_raw as $term_raw_item) {
                    preg_match("/(.+)__(.+)/", $term_raw_item, $term_match_array);
                    $term_name_real[] = $term_match_array[2];
                }
            }
            
            $args = array( 
                'post_type' => 'post', 
                'posts_per_page' => $atts['post_count'],
                'orderby' => 'date',
                'tax_query' => array(array(
                    'taxonomy'  => $taxonomy_name_real,
                    'field'     => 'slug',
                    'terms'     => $term_name_real
                ) ));

            $posts = get_posts($args);
            foreach($posts as $post){
                setup_postdata( $post );
                // Post Object Looks Like
                // WP_Post Object ( [ID] => 9 [post_author] => 1 [post_date] => 2018-02-27 22:11:15 [post_date_gmt] => 2018-02-28 03:11:15 [post_content] => 
                // [post_title] => Russia's Disinformation Offensive [post_excerpt] => [post_status] => publish [comment_status] => open [ping_status] => open 
                // [post_password] => [post_name] => russias-disinformation-offensive [to_ping] => [pinged] => [post_modified] => 2018-03-27 20:49:28 [post_modified_gmt] => 2018-03-28 01:49:28 
                // [post_content_filtered] => [post_parent] => 0 [guid] => http://localhost/asd-wp/?p=9 [menu_order] => 0 [post_type] => post [post_mime_type] => [comment_count] => 0 
                // [filter] => raw )
                
                // get post categories
                $categories = get_the_category($post->ID);

                // get the tags
                $tags = get_the_tags($post->ID);
                
                if($tags)
                {
                    foreach ($tags as $tag)
                    {
                        $tag_text .= $tag->name . ", ";
                    }
                }

                $tag_text = rtrim($tag_text,", ");

                // get the post featured image
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
                // $featured_image = get_the_post_thumbnail($post->ID,"",array( 'class' => 'article__image' ));
                $featured_image = "<img src='<?php echo $image; ?>' class='article__image' />";
            
                // start the article container
                $html .= "<article class='asd-article asd-article--featured'>";
                $html .= $featured_image;
                $html .= "<div class='article__content'>";
                $html .= '<h4 class="article__type">' . $categories[0]->name . '</h4>';
                $html .= '<p class="article__tags">' . $tag_text . '</p>';
                $html .= '<h3 class="article__title"><a href="' . get_post_permalink($post->ID, true) . '">' . $post->post_title . '</a></h3>';
                $html .= '<p class="article__byline">By ';
                if(function_exists("get_coauthors")) {
                    $coauthors = get_coauthors($post->ID);
                    if(is_array($coauthors) ) {
                        
                        foreach($coauthors as $theAuthor) {
                            $html .= coauthors_posts_links_single($theAuthor) . " and ";
                        }

                        $html = rtrim($html," and ");
                    }
                }
                $html .= "</p>";
                
                // add excerpt
                if($atts['excerpt']) {
                    $html .= '<div class="article__excerpt"><a href="' . get_post_permalink($post->ID, true) . '">';
                    if ( has_excerpt($post) ) {
                        $the_excerpt = get_the_excerpt($post);
                        
                        $the_excerpt = substr($the_excerpt,0, (($atts['excerpt_length']) ? $atts['excerpt_length'] : $limit)+1);
                        
                        $the_excerpt = rtrim($the_excerpt) . "...";
                        $html .= '<p>' . $the_excerpt . '</p>';
                    } else {
                        // $the_excerpt = self::get_my_excerpt_by_id($post->ID,$atts['excerpt_length'],true);
                        
                        
                        $html .= $the_excerpt;
                    }
                    $html .= '</a></div>';
                }
               
                $html .= "</div></article>";
            // endwhile;endif;
            }

            $html .= "</div>";
            return $html;
        }

        // function get_my_excerpt_by_id($post_id, $length, $strip_html){
        //     if(!$length)
        //         $excerpt_length = 35;
        //     else
        //         $excerpt_length = $length;
        
        //     if(!isset($strip_html) || $strip_html == true)
        //         $strip_html = true;
        
        //     $the_post = get_post($post_id); //Gets post ID
        //     $the_excerpt = apply_filters('the_content',$the_post->post_content);//Gets post_content to be used as a basis for the excerpt
        
        //     if($strip_html == true)
        //     {
        //         $the_excerpt = strip_tags(strip_shortcodes($the_excerpt), '<style>');
        //         $the_excerpt = preg_replace("|<style\b[^>]*>(.*?)</style>|s", "", $the_excerpt);
        //         $the_excerpt = strip_tags($the_excerpt);
        //     }
        
        //     $words = explode(' ', $the_excerpt, $excerpt_length + 1);
        
        //     if(count($words) > $excerpt_length) :
        //         array_pop($words);
        //         array_push($words, '…');
        //         $the_excerpt = implode(' ', $words);
        //     endif;
        
        //     return $the_excerpt;
        // }

        /** 
         * Process the must run when plugin is activated
         * 
         * @static
         * @access public
         * @since 1.0
         */
        public static function activation() {
            if ( !class_exists( 'FusionBuilder')) {
                echo '<style type="text/css">#error-page > p{display:-webkit-flex;display:flex;}#error-page img {height: 120px;margin-right:25px;}.fb-heading{font-size: 1.17em; font-weight: bold; display: block; margin-bottom: 15px;}.fb-link{display: inline-block;margin-top:15px;}.fb-link:focus{outline:none;box-shadow:none;}</style>';
                $message = '<span><span class="fb-heading">Sample Addon for Fusion Builder could not be activated</span>';
                $message .= '<span>ASD Featured Post for Fusion Builder can only be activated if Fusion Builder 1.0 or higher is activated. Click the link below to install/activate Fusion Builder, then you can activate this plugin.</span>';
                $message .= '<a class="fb-link" href="' . admin_url( 'admin.php?page=avada-plugins' ) . '">' . esc_attr__( 'Go to the Avada plugin installation page', 'Avada' ) . '</a></span>';
                wp_die( wp_kses_post( $message ) );
            } else {
                // do things if we are loading plugin
            }
        }


  }