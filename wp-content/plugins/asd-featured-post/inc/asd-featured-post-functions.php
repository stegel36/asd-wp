<?php
    function asdfp_avada_render_post_metadata( $layout, $settings = array() ) {
        global $fusion_settings;

        $fusion_settings = ($fusion_settings) ? $fusion_settings : Fusion_Settings::get_instance();

        $html = $author = $date = $metadata = '';

        $settings = ( is_array( $settings)) ? $settings : array();

        $default_settings = array(
            'post_show_excerpt' => false
        );

        $html = "<p>render</p>";
        return apply_filters( 'fusion_post_metadata_markup', $html );

    }