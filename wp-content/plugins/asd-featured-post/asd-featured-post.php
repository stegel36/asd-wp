<?php
/**
 * Plugin Name: ASD Featured Post
 * Plugin URI: https://bitbucket.org/stegel36/asd-featured-post
 * Description: Add the ability to embed a mapbox map and content in a page
 * Version: 1.0
 * Author: AJ Siegel
 * Author URI: https://bitbucket.org/stegel36/
 * 
 * @package ASD Featured Post
 */

 # Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

 if( ! defined('ASD_FEATURED_POST_PLUGIN_DIR')) {
     define('ASD_FEATURED_POST_PLUGIN_DIR', plugin_dir_path(__FILE__));
 }

 require_once(plugin_dir_path(__FILE__) . 'asd-featured-post-activation.php');
 require_once(plugin_dir_path(__FILE__) . 'inc/asd-featured-post-functions.php');
//  register_activation_hook(__FILE__, array('ASD_Featured_Post', 'activation'));

// load shortcode elements
function asdfp_init_shortcodes() {
    require_once(plugin_dir_path(__FILE__) . 'inc/fusion-featured-post.php');
}

add_action('fusion_builder_shortcodes_init', 'asdfp_init_shortcodes');

// function add_new_shortcode($fusion_builder_enabled_elements) {
//     $fusion_builder_enabled_elements[] = 'fusion_asd_featured_post';

//     return $fusion_builder_enabled_elements;
// }

class AvadaASDFPHelper {

    //returns custom post type, taxonomies and terms in one array
    public static function all_custom_data() {

        //Array to be returned by the function
        $all_post_types_taxonomy=array();

        // create array for post type options
        $conf_pt= array (
            'public' => true
        );

        // get registered custom post types
        $custom_post_types_default = array('post'=> 'post');
        $custom_post_types_registered = get_post_types( $conf_pt, 'names', 'and');
        $custom_post_types = array_merge($custom_post_types_default, $custom_post_types_registered);


        //assign taxonomies to posts
        $args_terms = array(
            'orderby'    => 'count',
            'order' => 'DESC',
            'hide_empty' => 0,
        );

        foreach ($custom_post_types as $custom_post_type) {

            //get the list of taxonomies for the post tyep
            $taxonomy_objects = get_object_taxonomies( $custom_post_type);

            //get the list of terms object for the taxonomy
            unset($custom_taxonomies);
            $custom_taxonomies = array();
            foreach ($taxonomy_objects as $taxonomy_object) {

                $terms = get_terms($taxonomy_object, $args_terms);
                //cretae the array of taxonomies
                $custom_taxonomies[$taxonomy_object] = $terms;
            }

            //create the array of post types
            $all_post_types_taxonomy[$custom_post_type]= $custom_taxonomies;
        }
        return $all_post_types_taxonomy;
        //	return $custom_taxonomies;

    }

    //returns custom post types array
    public static function get_post_types() {

        $all_custom_array = self::all_custom_data();
        $custom_post_types_formatted=array();

        foreach ($all_custom_array as $custom_post_type => $taxonomy){
            $custom_post_types_formatted[$custom_post_type] = esc_attr__($custom_post_type, 'fusion-core');
        }
        return $custom_post_types_formatted;
    }

    /** 
     * Get Taxonomies 
     */
    public static function get_taxonomies_list() {
        
        //build the list of built in taxonomies
        $args_tax = array(
        'public'   => true,
        '_builtin' => false
        );

        $built_in_taxonomies = get_taxonomies( $args_tax, 'names', 'and');

        $indexed_array_built_in_tax=array();
        foreach ($built_in_taxonomies as $built_in_taxonomy =>$some_value) {
            $indexed_array_built_in_tax[]=$built_in_taxonomy;
        }

        $all_custom_array = self::all_custom_data();
        $custom_taxonomy_formatted=array();
        $custom_taxonomy_formatted['xxx__select_taxonomy'] = esc_attr__('Select Taxonomy', 'fusion-core');
        foreach ($all_custom_array as $custom_post_type => $taxonomies){

            foreach ($taxonomies as $taxonomy => $terms) {

                //filter built_in taxonomies
                if (in_array($taxonomy, $indexed_array_built_in_tax)) {
                    //continue;
                }
                
                $custom_taxonomy_formatted[$custom_post_type.'__'.$taxonomy] = esc_attr__($taxonomy, 'fusion-core');
            }
        }

        return $custom_taxonomy_formatted;
    }

    //	return custom terms
    public static function get_custom_terms() {
        $all_custom_array = self::all_custom_data();
        
        $custom_terms_formatted=array();
        foreach ($all_custom_array as $custom_post_type => $taxonomies){
        
            foreach ($taxonomies as $taxonomy => $terms) {
        
                foreach ( $terms as $term) {
                    $custom_terms_formatted[$taxonomy.'__'.$term->slug] = esc_attr__($term->name.' ('.$term->count.')', 'fusion-core');
                }
        
            }
        
        }
        return $custom_terms_formatted;
    
    }
}
 
//  if( ! class_exists( 'ASD_Featured_Post')) {

//     // include the class
//     include_once wp_normalize_path( ASD_FEATURED_POST_PLUGIN_DIR . '/inc/class-asd-featured-post.php');

//     /**
//      * Instnatiate the class
//      */
//     function asd_featured_post_activate() {
//         ASD_FEATURED_POST::get_instance();
//     }

//     add_action('wp_loaded', 'asd_featured_post_activate', 10);
//  }

//  /**
//   * Map shortcode to Fusion Builder
//   * @since 1.0
//   */
//   function map_asd_featured_post() {
//     $post_types = ASD_FEATURED_POST::get_post_types();
//     $taxonomies = ASD_FEATURED_POST::get_taxonomies_list();
//     $custom_terms = ASD_FEATURED_POST::get_custom_terms();

//     // Map settings for parent shortcode
//     fusion_builder_map(
//         array(
//             'name'          => esc_attr__( 'ASD Featured Post', 'fusion-builder'),
//             'shortcode'     => 'fusion_asd_featured_post',
//             'icon'          => 'fa fa-map',
//             'preview'       =>  ASD_FEATURED_POST_PLUGIN_DIR . 'preview/asd-featured-post-preview.php',
//             'preview_id'    => 'fusion-builder-block-module-asd-featured-post-template',
//             'params'        => array(
//                 array(
//                     'type'          => 'textfield',
//                     'heading'       => esc_attr__( 'Post Count', 'fusion-builder'),
//                     'description'   => esc_attr__( 'Enter the number of posts to display', 'fusion-builder'),
//                     'param_name'    => 'post_count',
//                     'value'         => '1'
//                 ),
//                 array(
//                     'type' => 'select',
//                     'heading' => esc_attr__('Post type', 'fusion-builder'),
//                     'description' => esc_attr__('Choose your custom post type or leave post for standard Blog.', 'fusion-builder'),
//                     'param_name' => 'cpt_post_type',
//                     'default' => 'post',
//                     'value' => $post_types,
//                 ),
//                 array(
//                     'type' => 'select',
//                     'heading' => esc_attr__('Custom Taxonomy', 'fusion-builder'),
//                     'description' => esc_attr__('Select a custom taxonomy if you want to filter by taxonomy terms.', 'fusion-builder'),
//                     'param_name' => 'cus_taxonomy',
//                     'default' => 'select_taxonomy',
//                     'value' => $taxonomies,
//                 ),
//                 array(
//                     'type' => 'multiple_select',
//                     'heading' => esc_attr__('Custom Term', 'fusion-builder'),
//                     'description' => esc_attr__('Select terms in selected custom taxonomy.', 'fusion-builder'),
//                     'param_name' => 'cus_terms',
//                     'value' => $custom_terms,
//                     'default' => '',
//                 ),
//                 array(
// 					'type'        => 'radio_button_set',
// 					'heading'     => esc_attr__( 'Show Excerpt', 'fusion-builder' ),
// 					'description' => esc_attr__( 'Controls if the post excerpt is displayed.', 'fusion-builder' ),
// 					'param_name'  => 'excerpt',
// 					'value'   => array(
// 						'yes'   => esc_attr__( 'Show', 'fusion-builder' ),
// 						'no'    => esc_attr__( 'Hide', 'fusion-builder' )
// 					),
// 					'default' => 'no',
//                 ),
//                 array(
// 					'type'        => 'range',
// 					'heading'     => esc_attr__( 'Excerpt Length', 'fusion-builder' ),
// 					'description' => esc_attr__( 'Insert the number of words/characters you want to show in the excerpt.', 'fusion-builder' ),
// 					'param_name'  => 'excerpt_length',
// 					'min'         => '0',
// 					'max'         => '500',
//                     'step'        => '1',
//                     'value'       => '50',
// 					'dependency'  => array(
// 						array(
// 							'element'  => 'excerpt',
// 							'value'    => 'no',
// 							'operator' => '!=',
// 						)
// 					),
// 				),
//             )  
//         )
//     );
//   }

//   add_action( 'fusion_builder_before_init', 'map_asd_featured_post', 11);




//   /**
//    * Include options from options folder
//    * 
//    * @access public
//    * @since 1.0
//    * @return void
//    */
//   function fusion_init_asd_featured_post_options() {

//     // Early exit if the class doesn't exist
//     if (! class_exists( 'Fusion_Elemet')) {
//         return;
//     }

//     require_once 'options/class-asd-featured-post-options.php';

//     // Instantiate the object
//     new ASD_Featured_Post();
//   }

//   add_action('fusion_builder_shortcodes_init', 'fusion_init_asd_featured_post_options', 1);