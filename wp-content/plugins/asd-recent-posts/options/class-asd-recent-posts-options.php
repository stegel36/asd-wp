<?php
/**
 * Add element options to Fusion-Builder.
 *
 * @since 1.0
 * @package ASD Recent Posts for Fusion Builder
 */

/**
 * Sample options class.
 *
 * @package Fusion-Builder-ASD-Recent-Posts
 * @since 1.0
 */
class ASD_Recent_Posts_Options extends Fusion_Element {

    /** 
     * Constructor
     * 
     * @access public
     * @since 1.0
     */
    public function __construct(){
        parent::__construct();        
    }

    /**
     * Add settings to element options panel
     * 
     * @access public
     * @since 1.0
     * @return array $section Settings
     */

     public function add_options() {
        return array(
            'asd_mapbox_shortcode_section' => array(
                'label'        => esc_html__(' ASD Recent Posts', 'fusion-builder'),
                'description'  => 'Settings for ASD Recent Posts',
                'id'           => 'asd_recent_posts_shortcode_section',
                'type'         => 'sub-section',
                'fields'       => array(
                   'dropdown_field' => array(
                       'label'       => esc_html__( 'Sample Dropdown Field', 'fusion-builder' ),
                       'description' => esc_html__( 'This is just an example of drpdown setting field.', 'fusion-builder' ),
                       'id'          => 'dropdown_field',
                       'default'     => '1',
                       'type'        => 'select',
                       'choices'     => array(
                           '1' => esc_html__( 'Option 1', 'fusion-builder' ),
                           '2' => esc_html__( 'Option 2 with "Quotes"', 'fusion-builder' ),
                           '3' => esc_html__( 'Option 3', 'fusion-builder' ),
                       )
                   )
                )
            )
        );
     }
}