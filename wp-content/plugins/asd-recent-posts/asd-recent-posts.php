<?php
/**
 * Plugin Name: ASD Recent Posts
 * Plugin URI: https://bitbucket.org/stegel36/asd-mapbox
 * Description: Add the ability to embed a mapbox map and content in a page
 * Version: 1.0
 * Author: AJ Siegel
 * Author URI: https://bitbucket.org/stegel36/
 * 
 * @package ASD Recent Posts
 */

 if( ! defined('ASD_RECENT_POSTS_PLUGIN_DIR')) {
     define('ASD_RECENT_POSTS_PLUGIN_DIR', plugin_dir_path(__FILE__));
 }

 register_activation_hook(__FILE__, array('ASD_Recent_Posts', 'activation'));
 
 if( ! class_exists( 'ASD_Recent_Posts')) {

    // include the class
    include_once wp_normalize_path( ASD_RECENT_POSTS_PLUGIN_DIR . '/inc/class-asd-recent-posts.php');

    /**
     * Instnatiate the class
     */
    function asd_recent_posts_activate() {
        ASD_Recent_Posts::get_instance();
    }

    add_action('wp_loaded', 'asd_recent_posts_activate', 10);
 }

 /**
  * Map shortcode to Fusion Builder
  * @since 1.0
  */
  function map_asd_recent_posts() {

    $post_types = ASD_Recent_Posts::get_post_types();
    $taxonomies = ASD_Recent_Posts::get_taxonomies_list();
    $custom_terms = ASD_Recent_Posts::get_custom_terms();
    // Map settings for parent shortcode
    fusion_builder_map(
        array(
            'name'          => esc_attr__( 'ASD Recent Posts', 'fusion-builder'),
            'shortcode'     => 'fusion_asd_recent_posts',
            'icon'          => 'fa fa-map',
            'preview'       =>  ASD_RECENT_POSTS_PLUGIN_DIR . 'preview/asd-recent-posts-preview.php',
            'preview_id'    => 'fusion-builder-block-module-asd-recent-posts-template',
            'params'        => array(
                array(
                    'type'          => 'textfield',
                    'heading'       => esc_attr__( 'Post Count', 'fusion-builder'),
                    'description'   => esc_attr__( 'Enter the number of posts to display', 'fusion-builder'),
                    'param_name'    => 'post_count',
                    'value'         => ''
                ),
                array(
                    'type' => 'select',
                    'heading' => esc_attr__('Post type', 'fusion-builder'),
                    'description' => esc_attr__('Choose your custom post type or leave post for standard Blog.', 'fusion-builder'),
                    'param_name' => 'cpt_post_type',
                    'default' => 'post',
                    'value' => $post_types,
                ),
                array(
                    'type' => 'select',
                    'heading' => esc_attr__('Custom Taxonomy', 'fusion-builder'),
                    'description' => esc_attr__('Select a custom taxonomy if you want to filter by taxonomy terms.', 'fusion-builder'),
                    'param_name' => 'cus_taxonomy',
                    'default' => 'select_taxonomy',
                    'value' => $taxonomies,
                ),
                array(
                    'type' => 'multiple_select',
                    'heading' => esc_attr__('Custom Term', 'fusion-builder'),
                    'description' => esc_attr__('Select terms in selected custom taxonomy.', 'fusion-builder'),
                    'param_name' => 'cus_terms',
                    'value' => $custom_terms,
                    'default' => '',
                ),
                array(
                    'type' => 'multiple_select',
                    'heading' => esc_attr__('Exclude Taxonomies', 'fusion-builder'),
                    'description' => esc_attr__('Select custom taxonomies you want to exclude.', 'fusion-builder'),
                    'param_name' => 'cus_taxonomy_exc',
                    'value' => $taxonomies,
                ),
                array(
                    'type' => 'radio_button_set',
                    'heading' => esc_attr__('Show Photo', 'fusion-builder'),
                    'description' => esc_attr__('Display the post featured image.', 'fusion-builder'),
                    'param_name' => 'thumbnail',
                    'default' => 'yes',
                    'value' => array(
                        'yes' => esc_attr__('Yes', 'fusion-builder'),
                        'no' => esc_attr__('No', 'fusion-builder'),
                    ),
                ),
                array(
                    'type' => 'radio_button_set',
                    'heading' => esc_attr__('Show Author', 'fusion-builder'),
                    'description' => esc_attr__('Display the post\'s author.', 'fusion-builder'),
                    'param_name' => 'byline',
                    'default' => 'yes',
                    'value' => array(
                        'yes' => esc_attr__('Yes', 'fusion-builder'),
                        'no' => esc_attr__('No', 'fusion-builder'),
                    )
                ),
                array(
                    'type' => 'radio_button_set',
                    'heading' => esc_attr__('Show View All', 'fusion-builder'),
                    'description' => esc_attr__('Display the View All link.', 'fusion-builder'),
                    'param_name' => 'show_all',
                    'default' => 'yes',
                    'value' => array(
                        'yes' => esc_attr__('Yes', 'fusion-builder'),
                        'no' => esc_attr__('No', 'fusion-builder'),
                    )
                ),
                array(
					'type'        => 'link_selector',
					'heading'     => esc_attr__( 'Button URL', 'fusion-builder' ),
					'param_name'  => 'link',
					'value'       => '',
					'description' => esc_attr__( "Add the button's url ex: http://example.com.", 'fusion-builder' ),
				)

            )
        )
    );
  }

  add_action( 'fusion_builder_before_init', 'map_asd_recent_posts', 11);

  /**
   * Include options from options folder
   * 
   * @access public
   * @since 1.0
   * @return void
   */
  function fusion_init_asd_recent_posts_options() {

    // Early exit if the class doesn't exist
    if (! class_exists( 'Fusion_Elemet')) {
        return;
    }

    require_once 'options/class-asd-recent-posts-options.php';

    // Instantiate the object
    new ASD_Recent_Posts();
  }

  add_action('fusion_builder_shortcodes_init', 'fusion_init_asd_recent_posts_options', 1);