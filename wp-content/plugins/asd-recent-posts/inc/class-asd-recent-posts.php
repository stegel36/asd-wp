<?php
/**
 * This plugin creates a FusionBuilder element with MapBox, menu, icon and more
 * @since 1.2
 * @package ASD MapBox Addon for Fusion Builder
 */

 /**
  * The main plugin class
  */
  class ASD_Recent_Posts {
      /**
       * One instance of object
       * @static
       * @access private
       * @since 1.0
       * @var object
       */
      private static $instance;

      /**
       * Create or return instance of class
       * @static
       * @access public
       * @since 1.0
       */

       public static function get_instance() {
           // if not yer created and set to $instance create a new one
           if( null === self::$instance ) {
               self::$instance = new ASD_Recent_Posts();
           } 

           return self::$instance;
       }

       /**
        * Constructor
        * @since 1.0        
        */

        public function __construct() {
            add_action('wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

            add_shortcode( 'fusion_asd_recent_posts', array( $this, 'fusion_asd_recent_posts' ) );

            // New Settings Field to Fusion Builder
            // add_filter( 'fusion_builder_fields', array ( $this, 'add_new_field'));
        }

        /** Enqueue Scripts and styles 
         * @access public
         * @since 1.0
        */
        public function enqueue_scripts() {
            wp_enqueue_style( 'asd-recent-posts-css', plugins_url( 'css/recent-posts.css', __FILE__ ) );
            
        }

        //returns custom post type, taxonomies and terms in one array
        public static function all_custom_data() {

            //Array to be returned by the function
            $all_post_types_taxonomy=array();

            // create array for post type options
            $conf_pt= array (
                'public' => true
            );

            // get registered custom post types
            $custom_post_types_default = array('post'=> 'post');
            $custom_post_types_registered = get_post_types( $conf_pt, 'names', 'and');
            $custom_post_types = array_merge($custom_post_types_default, $custom_post_types_registered);


            //assign taxonomies to posts
            $args_terms = array(
                'orderby'    => 'count',
                'order' => 'DESC',
                'hide_empty' => 0,
            );

            foreach ($custom_post_types as $custom_post_type) {

                //get the list of taxonomies for the post tyep
                $taxonomy_objects = get_object_taxonomies( $custom_post_type);

                //get the list of terms object for the taxonomy
                unset($custom_taxonomies);
                $custom_taxonomies = array();
                foreach ($taxonomy_objects as $taxonomy_object) {

                    $terms = get_terms($taxonomy_object, $args_terms);
                    //cretae the array of taxonomies
                    $custom_taxonomies[$taxonomy_object] = $terms;
                }

                //create the array of post types
                $all_post_types_taxonomy[$custom_post_type]= $custom_taxonomies;
            }
            return $all_post_types_taxonomy;
            //	return $custom_taxonomies;

        }

        //returns custom post types array
    public static function get_post_types() {

        $all_custom_array = self::all_custom_data();
        $custom_post_types_formatted=array();

        foreach ($all_custom_array as $custom_post_type => $taxonomy){
            $custom_post_types_formatted[$custom_post_type] = esc_attr__($custom_post_type, 'fusion-core');
        }
        return $custom_post_types_formatted;
    }

         /** 
         * Get Taxonomies 
         */
        public static function get_taxonomies_list() {
           
            //build the list of built in taxonomies
            $args_tax = array(
            'public'   => true,
            '_builtin' => false
            );

            $built_in_taxonomies = get_taxonomies( $args_tax, 'names', 'and');

            $indexed_array_built_in_tax=array();
            foreach ($built_in_taxonomies as $built_in_taxonomy =>$some_value) {
                $indexed_array_built_in_tax[]=$built_in_taxonomy;
            }

            $all_custom_array = self::all_custom_data();
            $custom_taxonomy_formatted=array();
            $custom_taxonomy_formatted['xxx__select_taxonomy'] = esc_attr__('Select Taxonomy', 'fusion-core');
            foreach ($all_custom_array as $custom_post_type => $taxonomies){

                foreach ($taxonomies as $taxonomy => $terms) {

                    //filter built_in taxonomies
                    if (in_array($taxonomy, $indexed_array_built_in_tax)) {
                        //continue;
                    }
                    
                    $custom_taxonomy_formatted[$custom_post_type.'__'.$taxonomy] = esc_attr__($taxonomy, 'fusion-core');
                }
            }

            return $custom_taxonomy_formatted;
        }

        //	return custom terms
        public static function get_custom_terms() {
            $all_custom_array = self::all_custom_data();

            $custom_terms_formatted=array();
            foreach ($all_custom_array as $custom_post_type => $taxonomies){

                foreach ($taxonomies as $taxonomy => $terms) {

                    foreach ( $terms as $term) {
                        $custom_terms_formatted[$taxonomy.'__'.$term->slug] = esc_attr__($term->name.' ('.$term->count.')', 'fusion-core');
                    }

                }

            }
            return $custom_terms_formatted;

        }

        /**
         * Returns the content
         * 
         * @access public
         * @since 1.0
         * @param array $atts The attributes array
         * @param string $content The content.
         * @return string
         */
        public function fusion_asd_recent_posts($atts, $content ) {
            $unique_class = 'asd-' . rand();

             // get taxonomy name separate from post type
             if($atts['cus_taxonomy'] !== 'xxx__select_taxonomy') {
                 
                preg_match("/(.+)__(.+)/", $atts['cus_taxonomy'], $taxonomy_match_array);
                
                $taxonomy_name_real = $taxonomy_match_array[2];
    
                // get term name separate from taxonomy
                if (!empty($atts['cus_terms'])) {
                    $term_name_real = array();
                    $taxonomy_terms_raw = explode(',', $atts['cus_terms']);
                    
                    foreach ($taxonomy_terms_raw as $term_raw_item) {
                        preg_match("/(.+)__(.+)/", $term_raw_item, $term_match_array);
                        $term_name_real[] = $term_match_array[2];
                    }
                }
                $exc_taxonomies = explode(",",$atts['cus_taxonomy_exc']);
                $excludes = [];
                foreach($exc_taxonomies as $exc_taxonomy) {
                    preg_match("/(.+)__(.+)/", $exc_taxonomy, $taxonomy_exc_match_array);

                    $excludes[] = array(
                        "taxonomy" => $taxonomy_exc_match_array[2],
                        "operator" => "NOT EXISTS"
                    );
                }
                
                $tax_query = array (
                    'relation' => 'AND',
                    array(

                    'taxonomy'  => $taxonomy_name_real,
                    'field'     => 'slug',
                    'terms'     => $term_name_real
                )//,$excludes
                );
                
             }
             
            $html ="<div class='$unique_class'>";
            $args = array( 
                'post_type' => 'post', 
                'posts_per_page' => $atts['post_count'],
                'tax_query' => is_array($tax_query) ? array($tax_query) : ''
            );
            
            $posts = get_posts($args);

            foreach($posts as $post)
            {
                 // start the article container
                $html .= "<article class='asd-article asd-article--list'>";
                    
                // get the first category
                $categories = get_the_category($post->ID);
                
                if($atts['thumbnail'] === 'yes') {
                    // get the post featured image
                    $featured_image = get_the_post_thumbnail($post->ID,"full",array( 'class' => 'article__image' ));
                    $html .= $featured_image;
                }
                
                $html .= "<div class='article__content'>";
                $html .= "<p class='asd-article__category'>" . $categories[0]->name . "</p>";
                $html .= "<h4 class='asd-article__title'><a href='" . get_post_permalink($post->ID, true) . "'>" . $post->post_title . "</a></h4>";
                if($atts['byline'] === 'yes') {
                    if(function_exists("get_coauthors")) {
                        $coauthors = get_coauthors($post->ID);
                        if(is_array($coauthors) ) {
                            $html .= "<p class='asd-article__byline'>by ";
                            foreach($coauthors as $theAuthor) {
                                $html .= coauthors_posts_links_single($theAuthor) . " | ";
                                
                            }
                            $html = rtrim($html," | ");
                            $html .= "</p>";
                        }
                    } else {                        
                        $html .= "<p class='asd-article__byline'>by " . get_the_author($post->ID) . "</p>";
                    }
                    
                }

                $html .= "</div>";
                
                
                $html .= "</article>";
            }
           
            if( (count($posts) > 0) && ($atts['show_all'])) {
                $html .= "<a href='" . $atts['link'] . "' class='btn btn--primary btn--mobile-block'>View All</a>";
            }

            $html .= "</div>";
            return $html;
        }

        /** 
         * Process the must run when plugin is activated
         * 
         * @static
         * @access public
         * @since 1.0
         */
        public static function activation() {
            if ( !class_exists( 'FusionBuilder')) {
                echo '<style type="text/css">#error-page > p{display:-webkit-flex;display:flex;}#error-page img {height: 120px;margin-right:25px;}.fb-heading{font-size: 1.17em; font-weight: bold; display: block; margin-bottom: 15px;}.fb-link{display: inline-block;margin-top:15px;}.fb-link:focus{outline:none;box-shadow:none;}</style>';
                $message = '<span><span class="fb-heading">Sample Addon for Fusion Builder could not be activated</span>';
                $message .= '<span>Sample Addon for Fusion Builder can only be activated if Fusion Builder 1.0 or higher is activated. Click the link below to install/activate Fusion Builder, then you can activate this plugin.</span>';
                $message .= '<a class="fb-link" href="' . admin_url( 'admin.php?page=avada-plugins' ) . '">' . esc_attr__( 'Go to the Avada plugin installation page', 'Avada' ) . '</a></span>';
                wp_die( wp_kses_post( $message ) );
            } else {
                // do things if we are loading plugin
            }
        }


  }
