<?php
/*
Plugin Name: ASD FacetWP - ASD Time Since
Description: "ASD Time Since" facet
Version: 1.0
Based on: 1.4.2
Author: AJ Siegel
Original Author URI: https://facetwp.com/
Original GitHub URI: facetwp/facetwp-time-since
*/

defined( 'ABSPATH' ) or exit;


/**
 * ASD_FacetWP registration hook
 */
add_filter( 'facetwp_facet_types', function( $facet_types ) {
    $facet_types['asd_time_since'] = new ASD_FacetWP_Facet_Time_Since();
    return $facet_types;
} );


/**
 * ASD Time Since facet class
 */
class ASD_FacetWP_Facet_Time_Since
{

    function __construct() {
        $this->label = __( 'ASD Time Since', 'fwp' );
    }


    /**
     * Parse the multi-line options string
     */
    function parse_choices( $choices ) {
        $choices = explode( "\n", $choices );
        foreach ( $choices as $key => $choice ) {
            $temp = array_map( 'trim', explode( '|', $choice ) );
            $choices[ $key ] = array(
                'label' => $temp[0],
                'format' => $temp[1],
                'seconds' => strtotime( $temp[1] ),
                'counter' => 0,
            );
        }

        return $choices;
    }


    /**
     * Is the format in the future?
     */
    function is_future( $format ) {
        if ( '+' == substr( $format, 0, 1 ) ) {
            return true;
        }
        elseif ( 'next' == substr( $format, 0, 4 ) ) {
            return true;
        }

        return false;
    }


    /**
     * Load the available choices
     */
    function load_values( $params ) {
        global $wpdb;

        $output = array();
        $facet = $params['facet'];
        $where_clause = $params['where_clause'];

        $sql = "
        SELECT f.facet_display_value
        FROM {$wpdb->prefix}facetwp_index f
        WHERE f.facet_name = '{$facet['name']}' $where_clause";
        $results = $wpdb->get_col( $sql );

        // Parse facet choices
        $choices = $this->parse_choices( $facet['choices'] );
        
        // Loop through the results
        foreach ( $results as $val ) {
            
            $post_time = (int) strtotime( $val );
            foreach ( $choices as $key => $choice ) {
                $choice_time = $choice['seconds'];
                
                // next week, etc.
                if ( $this->is_future( $choice['format'] ) ) {
                    if ( $post_time <= $choice_time && $post_time > time() ) {
                        $choices[ $key ]['counter']++;
                    }
                }
                // last week, etc.
                else {
                    if ( $post_time >= $choice_time && $post_time < time() ) {
                        $choices[ $key ]['counter']++;
                    }
                }
            }
        }

        // Return an associative array
        foreach ( $choices as $choice ) {
            if ( 0 < $choice['counter'] ) {
                $output[] = array(
                    'facet_display_value' => $choice['label'],
                    'counter' => $choice['counter'],
                );
            }
        }

        return $output;
    }


    /**
     * Generate the facet HTML
     */
    function render( $params ) {

        $output = '';
        $facet = $params['facet'];
        $values = (array) $params['values'];
        
        $selected_values = (array) $params['selected_values'];

        $label_any = empty( $facet['label_any'] ) ? __( 'Any', 'fwp' ) : $facet['label_any'];
        $label_any = facetwp_i18n( $label_any );
        
        $output .= '<select class="facetwp-dropdown facetwp-type-time_since">';
        $output .= '<option value="">' . esc_attr( $label_any ) . '</option>';
        
        foreach ( $values as $row ) {
            
            
            $display_value = '';
            
            // Determine whether to show counts
            $display_value .= esc_attr( $row['facet_display_value'] );
            $safe_value = FWP()->helper->safe_value( $display_value );
            $selected = in_array( $safe_value, $selected_values ) ? ' selected' : '';
            $show_counts = apply_filters( 'facetwp_facet_dropdown_show_counts', true, array( 'facet' => $facet ) );

            if ( $show_counts ) {
                $display_value .= ' (' . $row['counter'] . ')';
            }
            
            $output .= '<option value="' . esc_attr( $safe_value ) . '"' . $selected . '>' . $display_value . '</option>';
        }
        $output .= "</select>";

        return $output;
    }


     /**
     * Filter the query based on selected values
     */
    /**
     * Filter the query based on selected values
     */
    function filter_posts( $params ) {
        global $wpdb;

        $facet = $params['facet'];
        $selected_values = $params['selected_values'];
        $selected_values = is_array( $selected_values ) ? $selected_values[0] : $selected_values;

        $choices = $this->parse_choices( $facet['choices'] );

        foreach ( $choices as $key => $choice ) {
            $safe_value = FWP()->helper->safe_value( $choice['label'] );
            if ( $safe_value === $selected_values ) {
                $selected_values = date( 'Ymd', (int) $choice['seconds'] );

                if ( $this->is_future( $choice['format'] ) ) {
                    $where_clause = "facet_value <= '$selected_values' AND facet_value > NOW()";
                }
                else {
                    $where_clause = "facet_value >= '$selected_values' AND facet_value < NOW()";
                }

                $sql = "
                SELECT DISTINCT post_id FROM {$wpdb->prefix}facetwp_index
                WHERE facet_name = '{$facet['name']}' AND $where_clause";
                
                return $wpdb->get_col( $sql );
            }
        }

        return array();
    }


    /**
     * Output any admin scripts
     */
    function admin_scripts() {
?>
<script>
(function($) {
    wp.hooks.addAction('facetwp/load/asd_time_since', function($this, obj) {
        $this.find('.facet-source').val(obj.source);
        $this.find('.facet-choices').val(obj.choices);
        $this.find('.facet-source').val(obj.source);
        $this.find('.facet-label-any').val(obj.label_any);
        $this.find('.facet-parent-term').val(obj.parent_term);
        $this.find('.facet-orderby').val(obj.orderby);
        $this.find('.facet-hierarchical').val(obj.hierarchical);
        $this.find('.facet-count').val(obj.count);
    });

    wp.hooks.addFilter('facetwp/save/asd_time_since', function(obj, $this) {
        obj['source'] = $this.find('.facet-source').val();
        obj['choices'] = $this.find('.facet-choices').val();
        obj['source'] = $this.find('.facet-source').val();
        obj['label_any'] = $this.find('.facet-label-any').val();
        obj['parent_term'] = $this.find('.facet-parent-term').val();
        obj['orderby'] = $this.find('.facet-orderby').val();
        obj['hierarchical'] = $this.find('.facet-hierarchical').val();
        obj['count'] = $this.find('.facet-count').val();
        return obj;
    });
})(jQuery);
</script>
<?php
    }


    /**
     * Output any front-end scripts
     */
    function front_scripts() {
?>

<script>
(function($) {
    wp.hooks.addAction('facetwp/refresh/asd_time_since', function($this, facet_name) {
        
        var selected_values = [];
        $this.find(".facetwp-type-time_since option:selected").each(function() {
            var val = $(this).attr('value');
            if ('' != val) {
                selected_values.push(val);
            }
        });
        FWP.facets[facet_name] = selected_values;
        
    });

    wp.hooks.addFilter('facetwp/selections/asd_time_since', function(output, params) {
        console.log("selections");
        var labels = [];
        $.each(params.selected_values, function(idx, val) {
            var label = params.el.find('.facetwp-type-time_since[value="' + val + '"]').clone();
            label.find('.counts').remove();
            labels.push(label.text());
        });
        return labels.join(' / ');
    });

    $(document).on('change', '.facetwp-dropdown.facetwp-type-time_since', function() {
        var $this = $(this);
       
        FWP.autoload();
    });

        
})(jQuery);
</script>
<?php
    }


    /**
     * Output admin settings HTML
     */
    function settings_html() {
?>
         <tr>
            <td>
                <?php _e( 'Default label', 'fwp' ); ?>:
                <div class="facetwp-tooltip">
                    <span class="icon-question">?</span>
                    <div class="facetwp-tooltip-content">
                        Customize the first option label (default: "Any")
                    </div>
                </div>
            </td>
            <td>
                <input type="text" class="facet-label-any" value="<?php _e( 'Any', 'fwp' ); ?>" />
            </td>
        </tr>
        <tr>
            <td>
                <?php _e('Choices', 'fwp'); ?>:
                <div class="facetwp-tooltip">
                    <span class="icon-question">?</span>
                    <div class="facetwp-tooltip-content"><?php _e( 'Enter the available choices (one per line)', 'fwp' ); ?></div>
                </div>
            </td>
            <td><textarea class="facet-choices"></textarea></td>
        </tr>
<?php
    }
}
