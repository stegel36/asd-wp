<?php

/**
 * Add New Menu Item
 */

// create the hook
add_action("admin_menu","asd_ne_add_admin_menu");

// add the item
function asd_ne_add_admin_menu() {
    add_menu_page(
        "Execute Map Updates",
        "Map Updates",
        "manage_links",
        "asd-node-execute/includes/asd-ne-page.php",
        '',
        'dashicons-location-alt',
        26
    );
}