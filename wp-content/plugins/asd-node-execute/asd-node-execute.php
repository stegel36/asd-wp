<?php
/**
 * Plugin Name: ASD Node Execute
 * Description: Runs a node method on the backend every time it is clicked
 * Version: 1.0
 * Author: AJ Siegel
 * Author URI: https://bitbucket.org/stegel36/
 *
 * @package ASD Node Execute
 */

 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

 // Include asd-node-execute-functions.php, use require_once to stop the script if file is not found
require_once plugin_dir_path(__FILE__) . 'includes/asd-node-execute-functions.php';
