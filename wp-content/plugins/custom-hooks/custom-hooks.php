<?php
/*
Plugin Name: Custom Hooks
Plugin URI: https://facetwp.com/
Description: A container for custom hooks
Version: 1.0
Author: FacetWP, LLC
*/

add_filter( 'facetwp_sort_options', "fwp_configure_sort_options",10,2);

function fwp_configure_sort_options($options, $params) {
    
    $options['country_asc'] = array(
        'label' => 'Country (A to Z)',
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'country',
            'order' => 'ASC',
		)
	);
    $options['country_desc'] = array(
        'label' => 'Country (Z to A)',
        'query_args' => array(
            'orderby' => 'meta_value_num',
            'meta_key' => 'country',
            'order' => 'DESC',
		)
    );
    $options['default'] = array(
        'label' => 'Date (Newest)',
        'query_args' => array(
            'orderby' => 'date',
            'order' => 'DESC',
        )
        );
    
    unset($options['date_desc']);
    
	return $options;
}

        
// prest url vars on country page
add_filter( 'facetwp_preload_url_vars', function( $url_vars ) {
    if ( strstr(FWP()->helper->get_uri(),"country/") ) {
        
        $countryObject = explode("/",FWP()->helper->get_uri());
        $country = $countryObject[count($countryObject)-1];        
        $url_vars['country'] = array( str_replace("%20","-",$country) );
        
        return $url_vars;
    }
    else{
        return $url_vars;
    }
} );

add_action('wp_head', 'fwp_refresh_on_sort');

function fwp_refresh_on_sort() {
?>

    <script type="text/javascript">
	//<![CDATA[
        var scrollOnRefresh = true;
        var hostname = (location.hostname === "localhost") ? "https://localhost:3000" : "https://securingdemocracy.gmfus.org:3000";
        var apiPrefix = (location.hostname === "localhost") ? "/asd-wp/" : "/";
		(function($) {

            // do something when sort dropdowns, to override autorefresh being off
            $(document).on('change', '.facetwp-sort-select', function() {
                FWP.extras.sort = this.value;
                FWP.refresh();
            });

            // on load more, don't scroll to top
            $(document).on("click",".fwp-load-more", function() {
                
                scrollOnRefresh = false;
            });


            $(document).on("click", "#asd-search__expand", function() {
                $(".asd-search__container").collapseSearch();
            });
            <?php if(is_page("threats")) { ?>
                $(document).on('facetwp-loaded', function() {
                    
                    if(FWP.build_query_string().length > 0) {
                        $(".asd-search__container").collapseSearch();
                        $(" #results-page__title").text("Search Results");
                        $("#noteworthy").hide("medium");  
                        
                        const waiting = () => {
                            if (map.isStyleLoaded() && map.getSource("threats")) {
                                var queryString = "";
                                // update map
                                // check for threats
                                if (FWP.facets.threat.length > 0) {
                                    queryString += "?threat=" + FWP.facets.threat;
                                }
                                if (FWP.facets.country.length > 0) {
                                    queryString += (queryString.length >0) ? "&" : "?";
                                    queryString += "country=" + FWP.facets.country;
                                }

                                if (FWP.facets.date_slider.length > 0) {
                                    queryString += (queryString.length >0) ? "&" : "?";
                                    queryString += "startedAfter=" + FWP.facets.date_slider;
                                }
                                else if (FWP.facets.time.length > 0) {
                                    
                                    var startedAfter = moment();
                                    switch (FWP.facets.time[0]) {
                                        case "last-3-months":
                                            startedAfter = startedAfter.subtract(3,"M");
                                            break;
                                        case "last-6-months":
                                            startedAfter = startedAfter.subtract(6,"M");
                                            break;
                                        case "past-year":
                                            startedAfter = startedAfter.subtract(1,"y");
                                            break;
                                        default:
                                            startedAfter = startedAfter;
                                    }
                                    
                                    queryString += (queryString.length >0) ? "&" : "?";
                                    queryString += "startedAfter=" + startedAfter.format("YYYYMMDD");
                                }

                                if (FWP.facets.keywords.length > 0) {
                                    queryString += (queryString.length >0) ? "&" : "?";
                                    queryString += "keywords="  + FWP.facets.keywords;
                                }

                                    // if(i === 0)
                                    // {
                                    //     operationsMatch.push('match', ['get', 'operationSlug']);
                                    // }
                                    // includedOperations.push(el);
                                    
                                    // if(i === FWP.facets.operations.length-1) {
                                    
                                    //     operationsMatch.push(includedOperations);
                                    //     operationsMatch.push(true);
                                    //     operationsMatch.push(false);
                                    //     newFilter.push(operationsMatch);
                                    
                                    // }
                                //TODO: WORK ON REFOCUS
                                // var bounds = new (mapboxgl.LngLatBounds)
                                // map.setCenter([-8.78, 15.4]);
                                // map.setZoom(1.4);
                                

                                // (newFilter.length > 0) ? map.setFilter("threatLayer",newFilter) : "";
                                $.getJSON(hostname + "/api/v1/incidents/incidentsByCountry/" + queryString).then(function(response){
                                    map.getSource("threats").setData(response.data);

                                    if(FWP.facets.country.length !== 1 && response.data.features.length > 1)
                                    {
                                        var boundingBox = getBoundingBox(response.data);
                    
                                        map.fitBounds([[boundingBox.xMin, boundingBox.yMin], [boundingBox.xMax, boundingBox.yMax]],{
                                            padding : {
                                                top: 50,
                                                left: 175,
                                                bottom: 50,
                                                right: 175
                                            }
                                        });
                                    }
                                    else {
                                        map.flyTo({center: response.data.features[0].geometry.coordinates});
                                    }
                                    

                                })
                                
                                // if(FWP.facets.started_after.length > 0) {
                                    
                                    // $.getJSON(window.apiPrefix + "wp-json/operations/v1/all/startDate=" + FWP.facets.started_after[0], function(response) {
                                    //     var country = "";
                                    //     var countryThreats = [];
                                    //     var results = [];
                                    //     var resultIndex = 0;
                                    //     response.forEach(function(el, i) {
                                    //         country = el.country;
                                            
                                    //         var countrySlug = slugify(country);
                                    //         resultIndex = results.map(function(o) { return o.properties.country; }).indexOf(country);

                                    //         if(resultIndex < 0) {
                                    //             results.push( {
                                    //                 type : "Feature",
                                    //                 geometry : {
                                    //                     type : "Point",
                                    //                     coordinates : [parseFloat(el.longitude), parseFloat(el.latitude)]
                                    //                 },
                                    //                 properties : {
                                    //                     country: country,
                                    //                     countrySlug : countrySlug,
                                    //                     count : 1
                                    //                 }
                                    //             });

                                    //             // setup countryThreats object
                                    //             countryThreats[country] = {};

                                    //             resultIndex = results.length - 1;
                                    //         } else{
                                    //             // if country exist, just increment count by 1
                                    //             results[resultIndex].properties.count = results[resultIndex].properties.count + 1;
                                    //         }

                                    //         // now add the threats to the group
                                    //         if (el.threats.length > 0 ) {
                                    //             el.threats.forEach(function(threat, j) {
                                    //                 var threatSlug = threat.slug;

                                    //                 // check if threat is in country
                                    //                 if( !(threatSlug in countryThreats[country])){
                                    //                     countryThreats[country][threatSlug] = {
                                    //                         count : 1,
                                    //                         slug : threatSlug,
                                    //                         threat : threat.name
                                    //                     }
                                    //                 } else {
                                    //                     countryThreats[country][threatSlug].count = countryThreats[country][threatSlug].count + 1;
                                    //                 }
                                    //                 results[resultIndex].properties[threatSlug] = true;
                                    //                 results[resultIndex].properties.threats = countryThreats[country];
                                    //             });

                                    //             // sort the threats by the threat name
                                    //             var newObject = {};
                                    //             Object.keys(results[resultIndex].properties.threats).sort().forEach(function(key) {
                                    //                 newObject[key] = results[resultIndex].properties.threats[key];
                                    //             });

                                    //             // update the countries with threats.
                                    //             results[resultIndex].properties.threats = newObject;
                                    //         };
                                    //     });
                                    //     // create the JSON object
                                    //     var geoJSON = {
                                    //         "type" : "FeatureCollection",
                                    //         "features" : results
                                    //     };

                                    //     console.log(geoJSON);
                                    //     map.getSource("threats").setData(geoJSON);
                                    // });

                                    
                                // }
                                
                                // var filteredFeatures = map.querySourceFeatures("operations", { filter:newFilter});
                                
                                // var collection = {features : filteredFeatures};
                                
                                // var boundingBox = getBoundingBox(collection);
                                
                                // map.fitBounds([[boundingBox.xMin, boundingBox.yMin], [boundingBox.xMax, boundingBox.yMax]],{
                                //     padding : {
                                //         top: 50,
                                //         left: 175,
                                //         bottom: 50,
                                //         right: 175
                                //     }
                                // });
                                // map.querySourceFeatures("operations", { filter:newFilter}).forEach(function(feature) {
                                //         bounds.extend(feature.geometry.coordinates);
                                // });
                                // map.fitBounds(bounds,{
                                //     padding : {
                                //         top: 50,
                                //         left: 175,
                                //         bottom: 50,
                                //         right: 175
                                //     }
                                // });
                                // collection = {};
                                // filteredFeatures = [];
                                // boundingBox = {};

                                
                            } else {                            
                                setTimeout(waiting, 200);
                            } 
                                
                        };
                        waiting();

                                    
                    }
                    else{
                        $("#noteworthy").show("medium");
                        $("#results-page__title").text("Recent Interference Incidents");

                        const waiting = () => {
                            if (!map.isStyleLoaded() || !map.getSource("operations")) {
                                setTimeout(waiting, 200);
                            } else {
                                $.getJSON(hostname + "/api/v1/incidents/incidentsByCountry", function(response) {
                                    map.getSource("threats").setData(response.data);

                                    var boundingBox = getBoundingBox(response.data);
                    
                                    map.fitBounds([[boundingBox.xMin, boundingBox.yMin], [boundingBox.xMax, boundingBox.yMax]],{
                                        padding : {
                                            top: 50,
                                            left: 175,
                                            bottom: 50,
                                            right: 175
                                        }
                                    });
                                });
                                //TODO: WORK ON REFOCUS
                                // var boundingBox = getBoundingBox(map.getSource("operations")._data); 


                                // map.fitBounds([[boundingBox.xMin, boundingBox.yMin], [boundingBox.xMax, boundingBox.yMax]],{
                                //     padding : {
                                //         top: 50,
                                //         left: 175,
                                //         bottom: 50,
                                //         right: 175
                                //     }
                                // });
                            }
                        };
                        waiting();
                    }
                    if((scrollOnRefresh) && (FWP.build_query_string().length > 0)) {
                        // $('html, body').animate({ scrollTop: 800 }, 500);
                    }
                    scrollOnRefresh = true;
                });
            <?php  } 
            
                ?>

            
            $.fn.collapseSearch = function() {
                // $(this).toggleClass("asd-search__container--sticky");
                // $(".asd-search__mini").toggleClass("asd-search__mini--collapsed");
                // var count = 0;
                // if (FWP.facets.keywords.length > 0) {
                //     $("#asd-search_keywords").toggleClass("asd-search__param--show").html("<span class='asd-search__term'>Keywords: </span>" + FWP.facets.keywords.toString() + " ");
                //     count++;
                // }
                
                // if (FWP.facets.threat.length > 0) {
                //     $("#asd-search_threats").toggleClass("asd-search__param--show").html(((count>0) ? "| " : "") + "<span class='asd-search__term'>Threats: </span>" + toTitleCase(FWP.facets.threat.toString()) + " ");
                // }
                // if (FWP.facets.country.length > 0) {
                //     $("#asd-search_countries").toggleClass("asd-search__param--show").html(((count>0) ? "| " : "") + "<span class='asd-search__term'>Countries: </span>" + toTitleCase(FWP.facets.country.join(", ")) + " ");
                // }
                // if (FWP.facets.operations.length > 0) {
                //     $("#asd-search_operations").toggleClass("asd-search__param--show").html(((count>0) ? "| " : "") + "<span class='asd-search__term'>Operations: </span>" + toTitleCase(FWP.facets.operations.toString ()) + " ");
                // }

                // if(!$(".asd-search__container").hasClass("asd-search__container--sticky")) {
                //     // console.log($(".asd-search__container")[0].offsetTop);
                //     $('html, body').animate({ scrollTop: $(".asd-search__container")[0].offsetTop }, 500);
                // }
                return this;
             }

             function toTitleCase(str) {
                 str = str.toLowerCase().split(" ");
                 for (var i=0; i < str.length; i++) {
                     str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
                 }
                 return(str.join(" "));
             }
        })(jQuery);

       
	//]]>
	</script>
<?php
} 

add_filter( 'facetwp_result_count', function( $output, $params ) {
    $output = $params['total'] . ' incidents';
    return $output;
}, 10, 2 );

add_filter( 'facetwp_sort_html', function( $html, $params ) {
    $html = '<select class="facetwp-sort-select facetwp-dropdown">';
    foreach ( $params['sort_options'] as $key => $atts ) {
        $html .= '<option value="' . $key . '">' . $atts['label'] . '</option>';
    }
    $html .= '</select>';
    return $html;
}, 10, 2 );

