<?php
/*
Plugin Name: ASD Author Avatars List
Description: Display lists of users using the author profile phot custom field using <a href="widgets.php">widgets</a> or <a href="https://authoravatars.wordpress.com/documentation/">shortcodes</a>. Requires ACF Pro
Version: 1.0
Author: AJ Siegel
*/

// The current version of the author avatars plugin. Needs to be updated every time we do a version step.
define( 'ASD_AUTHOR_AVATARS_VERSION', '1.00' );

// List of all version, used during update check. (Append new version to the end and write an update__10_11 method on ASDAuthorAvatars class if needed)
define( 'ASD_AUTHOR_AVATARS_VERSION_HISTORY', serialize( array(
	'1.0',
) ) );

require_once( 'lib/ASDAuthorAvatars.class.php' );
$aa = new ASDAuthorAvatars();

// can't add this in the class as it not found
add_action( 'wp_ajax_AAA_shortcode_paging', 'AAA_shortcode_paging' );
add_action( 'wp_ajax_nopriv_AAA_shortcode_paging', 'AAA_shortcode_paging' );

function AAA_shortcode_paging() {
	$nonce = $_POST['postCommentNonce'];
	// check to see if the submitted nonce matches with the
	// generated nonce we created earlier
	if ( ! wp_verify_nonce( $nonce, 'asd-author-avatars-shortcode-paging-nonce' ) ) {
		die( 'Busted!' );
	}
	// need to create class in the function scope
	$aaa = new ASDAuthorAvatars();
	$aaa->init_shortcodes();
	echo substr( str_replace( '<div class="shortcode-asd-author-avatars">', '', $aaa->author_avatars_shortcode->shortcode_handler( $_POST ) ), 0, - 6 );
	// echo	$aaa->author_avatars_shortcode->userlist->ajax_output();//. $aa->userlist->content .$aa->userlist->pagingHTML ;
	die();
}

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'AAA_add_action_links' );
function AAA_add_action_links( $links ) {
	$mylinks = array(
		sprintf( '<a href="%s" target="_blank">%s</a>',
			esc_url( 'https://translate.wordpress.org/projects/wp-plugins/asd-author-avatars' ),
			__( 'Help translate.', 'asd-author-avatars' )
		)
	);

	return array_merge( $links, $mylinks );
}