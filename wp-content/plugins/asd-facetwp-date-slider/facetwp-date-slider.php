<?php
/*
Plugin Name: ASD FacetWP - ASD Date Slider
Description: Facet for selecting dates via slider
Version: 1.1
Based on: 1.4.2
Author: AJ Siegel
Original Author URI: https://facetwp.com/
Original GitHub URI: facetwp/facetwp-time-since
*/

defined( 'ABSPATH' ) or exit;


/**
 * ASD_FacetWP registration hook
 */
add_filter( 'facetwp_facet_types', function( $facet_types ) {
    $facet_types['asd_date_slider'] = new ASD_FacetWP_Facet_Date_Slider();
    return $facet_types;
} );


/**
 * ASD Time Since facet class
 */
class ASD_FacetWP_Facet_Date_Slider
{

    function __construct() {
        $this->label = __( 'ASD Date Slider', 'fwp' );
    }


    /**
     * Parse the multi-line options string
     */
    // function parse_choices( $choices ) {
    //     $choices = explode( "\n", $choices );
    //     foreach ( $choices as $key => $choice ) {
    //         $temp = array_map( 'trim', explode( '|', $choice ) );
    //         $choices[ $key ] = array(
    //             'label' => $temp[0],
    //             'format' => $temp[1],
    //             'seconds' => strtotime( $temp[1] ),
    //             'counter' => 0,
    //         );
    //     }

    //     return $choices;
    // }


    /**
     * Is the format in the future?
     */
    function is_future( $format ) {
        if ( '+' == substr( $format, 0, 1 ) ) {
            return true;
        }
        elseif ( 'next' == substr( $format, 0, 4 ) ) {
            return true;
        }

        return false;
    }


    /**
     * Load the available choices
     */
    

    /**
     * Generate the facet HTML
     */
    function render( $params ) {
        $minDate =(strpos($params['values']['min'],"-") > 0) ? $params['values']['min'] : substr($params['values']['min'],0,4) . "-" . substr($params['values']['min'] ,4,2) . "-" . substr($params['values']['min'] ,6,2) ;
        $maxDate =(strpos($params['values']['max'],"-") > 0) ? $params['values']['max'] : substr($params['values']['max'],0,4) . "-" . substr($params['values']['max'] ,4,2) . "-" . substr($params['values']['max'] ,6,2);
        
        $output = '<div class="facetwp-date-slider-wrap">';
        $output .= '<input type="text" id="amount" class="facetwp-value facetwp-type-date-slider" />';
        $output .= "<div id='dateSlider'>";
        $output .= "<div id='slide-left' class='ui-slider-handle ui-slider-custom'><p></p></div>";
        $output .= "<div id='slide-right' class='ui-slider-handle ui-slider-custom'><p></p></div>";
        $output .= "</div>";
        $output .= '</div>';
        $output .= "<script type='text/javascript'>";
        $output .= "dateSlider.setDates('" . $minDate . "','" . $maxDate . "');";
        $output .= "</script>";
        
        
        
        return $output;
    }

    /* Load the available choices
     */
    function load_values ( $params ) {
        global $wpdb;
        $facet = $params['facet'];
        $sql = "
        SELECT MIN(facet_value) AS `min`, facet_display_value as `display` FROM {$wpdb->prefix}facetwp_index
        WHERE facet_name = '{$facet['name']}' ";
        // echo "sql: $sql";
        $row = $wpdb->get_row( $sql );
        
        $selected_min = isset( $selected_values[0] ) ? $selected_values[0] : $row->min;

        $endSource = explode("/",$facet['source_other']);
        $sql = "SELECT MAX(meta_value) as `max` FROM {$wpdb->prefix}postmeta WHERE meta_key='{$endSource[1]}'";
        // echo "sql2: $sql";
        $row = $wpdb->get_row( $sql );
        
        $selected_max = isset( $selected_values[1] ) ? $selected_values[1] : $row->max;
        $output = Array(
            "min" => $selected_min,
            "max" => $selected_max
        );

        return $output;
    }

     /**
     * Filter the query based on selected values
     */
    /**
     * Filter the query based on selected values
     */
    function filter_posts( $params ) {
        global $wpdb;

        $facet = $params['facet'];
        $values = $params['selected_values'];
        $where = '';
        
        $start = ( '' == $values[0] ) ? false : $values[0];
        $end = ( '' == $values[1] || 0 == $values[1]) ? false : $values[1];
        // echo "theend: $end";
        $is_dual = ! empty( $facet['source_other'] );
        $is_intersect = FWP()->helper->facet_is( $facet, 'compare_type', 'intersect' );

        /**
         * Intersect compare
         * @link http://stackoverflow.com/a/325964
         */
        if ( $is_dual && $is_intersect ) {
            $start = ( false !== $start ) ? date("Y-m-d", strtotime($start)) : '-999999999999';
            $end = ( false !== $end ) ? date("Y-m-d", strtotime($end)) : '999999999999';

            $where .= " AND facet_value <= '$end'";
            $where .= " AND facet_display_value >= '$start'";
        }
        else {
            if ( false !== $start ) {
                $start = date("Y-m-d", strtotime($start));
                $where .= " AND facet_value >= '$start'";
            }
            if ( false !== $end ) {
                $end = date("Y-m-d", strtotime($end));
                $where .= " AND facet_display_value <= '$end'";
            }
        }

        // echo $start;
        // $start = "2018-10-25";
        
        
        $sql = "
        SELECT DISTINCT post_id FROM {$wpdb->prefix}facetwp_index
        WHERE facet_name = '{$facet['name']}' $where";
        // echo $sql;
        return facetwp_sql( $sql, $facet );
    }


    /**
     * Output any admin scripts
     */
    function admin_scripts() {
?>

<script>
(function($) {
   
})(jQuery);
</script>

<?php
    }


    /**
     * Output any front-end scripts
     */
    function front_scripts() {
?>

<script>
(function($) {
    wp.hooks.addAction('facetwp/refresh/asd_date_slider', function($this, facet_name) {
        if(typeof FWP.facets.date_slider !== "undefined") {
            FWP.facets.time = "";
        }

        var selected_values = $("#amount").val();
        // $this.find(".facetwp-type-asd_date_slider input").each(function() {
        //     console.log("hey");
        //     var val = $(this).attr('value');
        //     if ('' != val) {
        //         selected_values.push(val);
        //     }
        // });
        // console.log($this);
        // selected_values.push($this.val());
        
        FWP.facets[facet_name] = (typeof selected_values !== "undefined" && selected_values.indexOf("undefined") < 0) ? selected_values : "";
        
    });

    // wp.hooks.addFilter('facetwp/selections/asd_date_slider', function(output, params) {
    //     console.log("selections");
    // //     var labels = [];
    // //     $.each(params.selected_values, function(idx, val) {
    // //         var label = params.el.find('.facetwp-type-time_since[value="' + val + '"]').clone();
    // //         label.find('.counts').remove();
    // //         labels.push(label.text());
    // //     });g
    // //     return labels.join(' / ');
    // });

    $(document).on('change', '#amount', function() {
        var $this = $(this);
            
        FWP.autoload();
    });

        
})(jQuery);
</script>
<?php
    }


    /**
     * (Admin) Output settings HTML
     */
    function settings_html() {
?>
        <div class="facetwp-row">
            <div>
                <?php _e('Other data source', 'fwp'); ?>:
                <div class="facetwp-tooltip">
                    <span class="icon-question">?</span>
                    <div class="facetwp-tooltip-content"><?php _e( 'Use a separate value for the upper limit?', 'fwp' ); ?></div>
                </div>
            </div>
            <div>
                <data-sources
                    :facet="facet"
                    :selected="facet.source_other"
                    :sources="$root.data_sources"
                    settingName="source_other">
                </data-sources>
            </div>
        </div>
        <div class="facetwp-row" v-show="facet.source_other">
            <div><?php _e('Compare type', 'fwp'); ?>:</div>
            <div>
                <select class="facet-compare-type">
                    <option value=""><?php _e( 'Basic', 'fwp' ); ?></option>
                    <option value="intersect"><?php _e( 'Intersect', 'fwp' ); ?></option>
                </select>
            </div>
        </div>
        
       
<?php
    }
}

/**
     * Output any front-end scripts
     */
    function facet_date_slider_scripts()
    {
        wp_enqueue_script('jquery-ui', plugins_url('assets/vendor/js/jquery-ui.js', __FILE__), array(), null, true);
        wp_enqueue_script('dateSlider', plugins_url('assets/js/dateSlider.js', __FILE__), array('jquery-ui'), null, true);
        wp_enqueue_style('jquery-ui.css', plugins_url('assets/vendor/css/jquery-ui.css', __FILE__), array(), '1.0');
        wp_enqueue_style('date-slider.css', plugins_url('assets/css/date-slider.css', __FILE__), array(), '1.0');
    }

    add_action('wp_enqueue_scripts', 'facet_date_slider_scripts',500);
