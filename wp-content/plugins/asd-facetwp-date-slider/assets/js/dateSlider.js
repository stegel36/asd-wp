var dateSlider = (function($) { 
    var minDate,maxDate;
    $(document).on('facetwp-loaded', function() {
        if(FWP.facets.hasOwnProperty("date_slider")) {

            var handleLeft = $('#slide-left p');
            var handleRight = $('#slide-right p');
            
            if (FWP.facets.date_slider.length === 0)
            {
                valueStartDate = moment(minDate);
                valueEndDate = moment(maxDate);
            } 
            else {

                if(!Array.isArray(FWP.facets.date_slider))
                {
                    value = FWP.facets.date_slider.split(',');
                    valueStartDate =  moment(value[0]);
                    valueEndDate = moment(value[1]);
                }
                else 
                {
                    valueStartDate =  moment(FWP.facets.date_slider[0]);
                    valueEndDate = moment(FWP.facets.date_slider[1]);
                }
            }
        $('#dateSlider').slider({
            range: true,
            min: moment(minDate).unix(),
            max: moment(maxDate).unix(),
            step: 86400,
            create: function() {
                handleLeft.text( moment.unix($( this ).slider('values',0)).format('MMM DD, YYYY'));
                handleRight.text( moment.unix($( this ).slider('values',1)).format('MMM DD, YYYY'));
            },
            values: [ valueStartDate.unix(),
            valueEndDate.unix()
            ],
            slide: function (event, ui ) {
                startDate = moment.unix(ui.values[ 0 ]).format('YYYYMMDD');
                endDate = moment.unix(ui.values[ 1 ]).format('YYYYMMDD');
                
                $( '#amount' ).val(startDate + ',' + endDate);
                $( '#amount' ).change();
                handleLeft.text(moment(startDate).format('MMM DD, YYYY'));
                if(!$("#toPresent").checked) {
                    handleRight.text(moment(endDate).format('MMM DD, YYYY'));
                }
                
            }
            }); 
            if(typeof FWP.facets.date_slider === 'string') {
                $('#amount').val(FWP.facets.date_slider);
            } else {                
                $('#amount').val(FWP.facets.date_slider[0] + ',' + FWP.facets.date_slider[1]);
            }
        }        
    });

    return {
        setDates : function(min,max) {
            minDate = min;
            maxDate = max;
        }
    }
  })(jQuery)