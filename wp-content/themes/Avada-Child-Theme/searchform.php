<?php
/**
 * The search-form template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>

	<div id="menu-search" class="fusion-search-form-content">
            <?php // echo facetwp_display( 'facet', 'keywords' ); ?>
            <!-- <button class="fwp-submit" data-href="search-results/"><i class="fa fa-search"></i></button> -->
			<form action="<?php echo get_site_url(); ?>/search-results"  method="get">
			<div class="facetwp-facet facetwp-facet-keywords facetwp-type-search" data-name="keywords" data-type="search">
				<span class="facetwp-search-wrap">
					<i class="facetwp-btn"></i>
					<!-- <input type="text" name="fwp_keywords" class="facetwp-search" value="" placeholder="Keyword Search"> -->
					<input type="text" placeholder="Search &hellip;" value="" name="fwp_keywords" class="facetwp-search">
				</span>
			</div>
			</form>
			<?php if ((!is_page("threats")) && (!is_page("country")) && (!is_page("search-results"))) : ?>
				<div style="display:none"><?php echo facetwp_display( 'template', 'threats2' ); ?></div>
			<?php endif; ?>
	</div>

