<?php
/**
 * Search & Filter Pro 
 *
 * ASD Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2015 Designs & Code
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() ) {

	if(!array_key_exists("s",$query->query))
	{
		$page_title = "Recent Threats";
	}
	else
	{
		$page_title = "Filtered Results (" . $query->found_posts . ")";
	}			
	?>

	
		<div class="results-page">
				<h2 class="results-page__title"><?php echo $page_title; ?></h2>
		</div>
	
	<!-- Found <?php //echo $query->found_posts; ?> Results<br />
	Page <?php //echo $query->query['paged']; ?> of <?php //echo $query->max_num_pages; ?><br /> -->
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<div class="asd-results">
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>
		<div class="asd-result">
			<?php 
				if ( has_post_thumbnail() ) {
					echo '<p class="asd-result__thumb">';
					the_post_thumbnail("small");
					echo '</p>';
				}

				// get category info
				$categories = get_the_category($post->ID);
			?>
            <p class='asd-result__category'><?php echo $categories[0]->name; ?></p>
			<h2 class="asd-result__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		
			<p class="asd-result__date"><?php the_date(); ?></p>
			
		</div>
		
		<?php
	}
	?>
	</div>
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />
	
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	<script>
		jQuery(document).ready(function() {
			jQuery('.sf-input-select').multiselect({
				header : false,
				menuHeight: "350px",
				menuWidth: "auto",
				noneSelectedText: "Filter by Threat",
				zIndex: "10000",
				appendTo: "document.body"
			});
		});

		jQuery(document).on("sf:ajaxfinish", ".searchandfilter", function(){
			jQuery('.sf-input-select').multiselect({
				header : false,
				menuHeight: "350px",
				menuWidth: "auto",
				noneSelectedText: "Filter by Threat",
				zIndex: "10000",
				appendTo: "document.body",
				selectedText: "Filter by Threat (#)"
			});
		});
	</script>
	<?php
}
else
{
	echo "No Results Found";
}
?>