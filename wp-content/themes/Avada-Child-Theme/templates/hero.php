<div class="threat-legend">
        <h2 class="threat-legend__title">Tool Type</h2>
        <ul class="threat-legend__list">
            <li class="threat-legend__item threat-legend__item--cyberattacks threat-legend__item--active" id="cyberattacks">Cyber Operations</li>
            <li class="threat-legend__item threat-legend__item--information-operations threat-legend__item--active" id="information-operations">Information Manipulation</li>
            <li class="threat-legend__item threat-legend__item--malign-finance threat-legend__item--active" id="malign-finance">Malign Finance</li>
            <li class="threat-legend__item threat-legend__item--political-and-social-subversion threat-legend__item--active" id="political-and-social-subversion">Civil Society Subversion</li>
            <li class="threat-legend__item threat-legend__item--strategic-economic-coercion threat-legend__item--active" id="strategic-economic-coercion">Economic Coercion</li>
            <li class="threat-legend__item threat-legend__item--multiple threat-legend__item--active" id="multiple">Multiple Threats</li>
            
        </ul>
    </div>
<!-- <div class="site-hero" id="site-hero">
    <h1 class="site-hero__tagline">Securing Democracy<br />is Not a Partisan Issue.</h1>
    <button id="whatsHappening" class="site-hero__cta btn btn--primary btn--large text-uppercase">See What's Happening</button>
    <div class="trend-bar">
      <div class="trend-hashtags flex flex-start">
            <h4 class="header--alt">Trending Russian Influence</h4>
            <ul class="trend-hashtags__list inline-list">
                <li><a href="#">#maga</a></li>
                <li><a href="#">#putin</a></li>
                <li><a href="#">#syria</a></li>
                <li><a href="#">#qanon</a></li>
                <li><a href="#">#mueller</a></li>
            </ul>
        </div>
        <div class="cta-hamilton flex flex-end">
            <h4 class="header-alt">Exposing the Russian Narrative.</h4>
            <button class="btn btn--primary">Launch Hamilton 68</button>
        </div>
    </div>
</div> -->
<div class="site-hero" id="site-hero">
    <?php
        $homeHero = get_page_by_path("home-no-map/home-hero");
        setup_postdata($homeHero);
        $content = fusion_get_post_content(3662,"no");
        echo $content;
    ?>
</div>