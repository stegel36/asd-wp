<div class="threat-legend">
    <h2 class="threat-legend__title">Tool Type</h2>
    <ul class="threat-legend__list">
        <li class="threat-legend__item threat-legend__item--cyberattacks threat-legend__item--active" id="cyber">Cyber Operations</li>
        <li class="threat-legend__item threat-legend__item--information-operations threat-legend__item--active" id="information-operations">Information Manipulation</li>
        <li class="threat-legend__item threat-legend__item--malign-finance threat-legend__item--active" id="malign-finance">Malign Finance</li>
        <li class="threat-legend__item threat-legend__item--political-and-social-subversion threat-legend__item--active" id="political-and-social-subversion">Civil Society Subversion</li>
        <li class="threat-legend__item threat-legend__item--state threat-legend__item--active" id="state">Economic Coercion</li>
    </ul>

</div>
<script type="text/javascript">
    (function($) {
        $(document).on('facetwp-loaded', function() {

    var jsonFile = "home.json";    
    
    $.getJSON(stylesheetDir + "/" + jsonFile, function(response) {

        var e = filterIncidents(response, FWP.facets ) 
        var base_url = window.location.origin;

            // get incidents for item
            var incidents = [];
            var incidentCount = 0;
            var noIncidents = true;
            var html = "";
            e.features.map(feature => {
                incidents = incidents.concat(feature.properties.incidents)
            });

            var actorCount = {};
            var actors = [];
            var toolCount = {};
            var tools = [];
            incidents.forEach(function(incident, i){
                if(incident.actors){
                    incident.actors.forEach(function(actor,i){
                        if(!actorCount.hasOwnProperty(actor.name)){
                            actorCount[actor.name] = 0;
                            actors.push(actor)
                        }
                        actorCount[actor.name]++;
                    });
                }
                if(incident.tools){
                    incident.tools.forEach(function(tool,i){
                        if(!toolCount.hasOwnProperty(tool.name)){
                            toolCount[tool.name] = 0;
                            tools.push(tool)
                        }
                        toolCount[tool.name]++;
                    });
                }
            });

            if(actors){
                html += "<div class='threat-legend-wrapper'><div class='threat-legend__title'>Threat Actor</div><ul class='threat-legend__list'>" 
                actors.forEach(function(actor,i){
                    html += "<li class='threat-legend__item threat-legend__item--" +  actor.slug  + " threat-legend__item--active' id='" +  actor.slug  + "'><a href='" + base_url + "/" + actor.taxonomy + "/" + actor.slug + "/'>"
                    html += "<span class='popup-meta-country'><img src='" + base_url + "/wp-content/uploads/2021/01/" + actor.slug + "-flag-square-icon-16.png' alt='"+actor.name+"'></span>"
                    html += actor.name + " - "+ actorCount[actor.name] +"</a></li>"
                })
                html += "</ul></div>" 
            }
            if(tools){
                html += "<div class='threat-legend-wrapper'><div class='threat-legend__title'>Tool</div><ul class='threat-legend__list'>" 
                tools.forEach(function(tool,i){                
                    html += "<li class='threat-legend__item threat-legend__item--" +  tool.slug  + " threat-legend__item--active' id='" +  tool.slug  + "'><a href='" + base_url + "/" + tool.taxonomy + "/" + tool.slug + "/'>" 
                    html += "<span class='popup-meta-tool'>"
                    html += " <div class='tool-icon-legend '><i class='glyphicon "
                    switch (tool.slug) {
                        case "malign-finance":
                            html += "fa-funnel-dollar"
                            break;
                        case "information-operations":
                            html += "fa-newspaper"
                            break;
                        case "cyberattacks":
                            html += "fa-laptop-code"
                            break;
                        case "strategic-economic-coercion":
                            html += "fa-chart-line"
                            break;
                        case "political-and-social-subversion":
                            html += "fa-user-secret"
                            break;
                    }
                    html += " fas' aria-hidden='true' title='"+tool.name+"'></i></div></span>"   
                    html += tool.name + " - "+ toolCount[tool.name] +"</a> </li>"
                })
                html += "</ul><div>" 
            }       
            $( ".threat-legend" ).html(html);


            });

            
    var filterIncidents = function(incidentsByCountry, filters){
        var filteredIncidents = {
            type: "FeatureCollection",
            features: []
        }
		
		
var africa = ["Algeria", "Angola", "Benin", "Botswana", "Burkina", "Burundi", "Cameroon", "Cape Verde", "Central African Republic", "Chad", "Comoros", "Congo", "Congo, Democratic Republic of", "Djibouti", "Egypt", "Equatorial Guinea", "Eritrea", "Ethiopia", "Gabon", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Ivory Coast", "Kenya", "Lesotho", "Liberia", "Libya", "Madagascar", "Malawi", "Mali", "Mauritania", "Mauritius", "Morocco", "Mozambique", "Namibia", "Niger", "Nigeria", "Rwanda", "Sao Tome and Principe", "Senegal", "Seychelles", "Sierra Leone", "Somalia", "South Africa", "South Sudan", "Sudan", "Swaziland", "Tanzania", "Togo", "Tunisia", "Uganda", "Zambia", "Zimbabwe"];
					
var nAmerica = ["Antigua and Barbuda", "Bahamas", "Barbados", "Belize", "Canada", "Costa Rica", "Cuba", "Dominica", "Dominican Republic", "El Salvador", "Grenada", "Guatemala", "Haiti", "Honduras", "Jamaica", "Mexico", "Nicaragua", "Panama", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Trinidad and Tobago", "United States"];
var sAmerica = ["Argentina", "Bolivia", "Brazil", "Chile", "Colombia", "Ecuador", "Guyana", "Paraguay", "Peru", "Suriname", "Uruguay", "Venezuela"];
var europe = ["Albania", "Andorra", "Armenia", "Austria", "Azerbaijan", "Belarus", "Belgium", "Bosnia and Herzegovina", "Bulgaria", "Croatia", "Cyprus", "CZ", "Denmark", "Estonia", "Finland", "France", "Georgia", "Germany", "Greece", "Hungary", "Iceland", "Ireland", "Italy", "Latvia", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Malta", "Moldova", "Monaco", "Montenegro", "Netherlands", "Norway", "Poland", "Portugal", "Romania", "San Marino", "Serbia", "Slovakia", "Slovenia", "Spain", "Sweden", "Switzerland", "Ukraine", "United Kingdom", "Vatican City"];
var oceania = ["Australia", "Fiji", "Kiribati", "Marshall Islands", "Micronesia", "Nauru", "New Zealand", "Palau", "Papua New Guinea", "Samoa", "Solomon Islands", "Tonga", "Tuvalu", "Vanuatu"];
var asia = ["Afghanistan", "Bahrain", "Bangladesh", "Bhutan", "Brunei", "Burma (Myanmar)", "Cambodia", "China", "East Timor", "India", "Indonesia", "Iran", "Iraq", "Israel", "Japan", "Jordan", "Kazakhstan", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Lebanon", "Malaysia", "Maldives", "Mongolia", "Nepal", "Oman", "Pakistan", "Philippines", "Qatar", "Russia", "Saudi Arabia", "Singapore", "Sri Lanka", "Syria", "Tajikistan", "Thailand", "Turkey", "Turkmenistan", "United Arab Emirates", "Uzbekistan", "Vietnam", "Yemen"];
		
		
        console.log(incidentsByCountry  , filters);

        filteredIncidents.features = incidentsByCountry.features.map( (countryIncidents) =>{
            //if not in selected countries, drop all incidents for that country
			//find applicable region
			var countryToFind = countryIncidents.properties.country;
			var region = africa.includes(countryToFind) ? "africa": 
							europe.includes(countryToFind) ? "europe" :							
			nAmerica.includes(countryToFind) ? "n.-america" :							
			sAmerica.includes(countryToFind) ? "s.-america" :							
			asia.includes(countryToFind) ? "asia" :							
			oceania.includes(countryToFind) ? "oceania" : "";
			
			
			//if not in selected countries, drop all incidents for that country
            if (filters.country.length > 0 && !(filters.country.includes(countryIncidents.properties.country.replaceAll(' ','-').toLowerCase()))){return null;}
            //console.log(countryIncidents, filters);
            countryIncidents.properties.incidents = countryIncidents.properties.incidents.filter((incident)=>{
                var keepThisIncident = true;
                //progressively apply filters 
                for (const [filterId, filterValues] of Object.entries(filters) ){
                    //not relevant filters
                    if(filterId in ['incidentCount', 'incident_pager_list', 'keywords','country']){keepThisIncident = keepThisIncident;}
                    if(filterId == 'tool' && filterValues.length > 0 ){
                        var tools = incident.tools.map( (tool) => {return tool.slug});
                        keepThisIncident = keepThisIncident &&  tools.some(r=> filterValues.includes(r))
                    }
                    if(filterId == 'regions' && filterValues.length > 0 ){
                        keepThisIncident = keepThisIncident &&  filterValues.includes(region)
                    }
                    if(filterId == 'threat_actor' && filterValues.length > 0 ){
                        var actors = incident.actors.map( (actor) => {return actor.slug});
                        keepThisIncident = keepThisIncident &&  actors.some(r=> filterValues.includes(r))
                    }
                    if(filterId == 'search_text' && filterValues.length > 0 ){
						//search title and excerpt
                        keepThisIncident = keepThisIncident && ( incident.title.toLowerCase().includes(filterValues.toLowerCase()) ||  incident.content.toLowerCase().includes(filterValues.toLowerCase()))    
					}
                    if(filterId == 'date_range' && filterValues.length > 0 ){
                        var rangeStart = filterValues[0].replaceAll('-','');
                        var rangeEnd = filterValues[1].replaceAll('-','');
                        if(rangeStart != ""){
                            keepThisIncident = keepThisIncident && incident.start_date[0] > rangeStart
                        }
                        if(rangeEnd != ""){
                            keepThisIncident = keepThisIncident && incident.end_date[0] < rangeEnd
                        }
                    }
                }
                return keepThisIncident;
            });
            //eliminate if we've filtered every incident
            if(countryIncidents.properties.incidents.length == 0 ){return null;}

            countryIncidents.properties.count = countryIncidents.properties.incidents.length;

            return countryIncidents;
        });
        filteredIncidents.features = filteredIncidents.features.filter(feature =>{ return feature != null});
        return filteredIncidents;
    }})
    })(jQuery);
</script> 