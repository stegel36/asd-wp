<?php
    $geoJSON = "threats.json";
    $interactive = 1;
?>
<script type="text/javascript">
    var stylesheetDir = "<?php echo get_stylesheet_directory_uri(); ?>";
    var restURL = "<?php echo get_rest_url(); ?>";
</script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/threats.js"></script>
<div class="map__container">
    <div id='map' class="threat-map"></div>
    </div>
