<?php
    $geoJSON = "home.json";
    $interactive = 0;
?>

<?php 
    
    class BISchema implements JsonSerializable
    {
        // protected $schema;
        protected $target;
        protected $operator;
        protected $values;

        public function __construct(array $data) {
            // $this->index = $data['index'];
            $target = new Target();
            // $this['$schema'] = "http://powerbi.com/product/schema#basic";
            $this->target = $target;
            $this->operator = "=";
            $this->values = array($data['id']);
        }

        public function jsonSerialize() {
            return get_object_vars($this);
        }    
    }

    class Target implements JsonSerializable
    {
        protected $table;
        protected $column;

        public function __construct() {
            $this->table = "Authoritarian Interference Tracker";
            $this->column = "Incident Index";
        }

        public function jsonSerialize() {
            return get_object_vars($this);
        }
    }
?>
<?php
    $incidentQueryArgs = array(
        'post_type' => 'asd_incidents',
        'post_status' => 'publish',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 1,
    );

    $incidentQuery = new WP_Query($incidentQueryArgs);

    if( $incidentQuery->have_posts() ) {
        $incidentQuery->the_post();
 //        $incidentIndex = get_post_meta( get_the_ID(), 'incident_index', true );
        $incidentIndex = get_the_ID();
        $schema = new BISchema(array("id" => $incidentIndex));
        $jsonString = json_encode(array($schema));
        // $jsonString = '[{"$schema":"http://powerbi.com/product/schema#basic","target":{"table":"Authoritarian Interference Tracker","column":"Incident Index"},"operator":"=","values": "['.$incidentIndex.']"}]';
        // $querystring = "[{"$schema":"http://powerbi.com/product/schema#basic","target":{"table":"Authoritarian Interference Tracker","column":"Incident Index"},"operator":"=","values":[' . $incidentIndex . ']"}]';
        // $url = site_url() . "/authoritarian-interference-tracker/?filters=%5B%7B'%24schema'%3A'http%3A%2F%2Fpowerbi.com%2Fproduct%2Fschema%23basic'%2C'target'%3A%7B'table'%3A'Authoritarian%20Interference%20Tracker'%2C'column'%3A'Incident%20Index'%7D%2C'operator'%3A'%3D'%2C'values'%3A%5B'$incidentIndex'%5D%7D%5D";
        ?>
        <script type="text/javascript">
            var latestThreatID = "<?php echo $incidentIndex; ?>";
            var latestThreatTitle = "<?php the_title(); ?>";
        </script>
    <div class="incident-alert incident-alert--hide">
        <div class="incident-alert__notice">Latest Threat</div>
        <div class="incident-alert__content">
          
        </div>
        <div class="incident-alert__close">
            <i class="fa fa-close"></i>
        </div>
    </div>
<?php 
}
?>
<div class="map__container">
    <div class="map-overlay map-overlay--alert <?php if(is_admin_bar_showing()) echo "map-overlay--admin"; ?>"></div>
    <div class="map-guide">
        <div class="map-guide__content">
            <?php
                $instructions = get_page_by_path("home-no-map/instructions");
                setup_postdata($instructions);
                
                $content = fusion_get_post_content(get_the_ID(),"no");
                echo $content;
                // $post_id = 2302;
                // // $queried_post = get_post($post_id);
                // $content = fusion_get_post_content($post_id,"no");
                // echo $content;
                // echo $queried_post->post_content;
                // $queried_post = "";
            ?>
        </div>
        <div class="map-guide__actions">
            <label><input type="checkbox" value="1" name="dontShow" id="dontShow" /> I don't want to see this again</label><button class="btn btn--primary map-guide__button">Close Guide</button>
        </div>

    </div>
    <?php
       $queried_post = get_page_by_path("/map-actions", OBJECT, "internal");
    //    echo $queried_post;
       echo apply_filters( 'the_content', $queried_post->post_content);
    ?>
    <div id='map' class="home-map"></div>
</div>
<script type="text/javascript">
    var stylesheetDir = "<?php echo get_stylesheet_directory_uri(); ?>";
    var restURL = "<?php echo get_rest_url(); ?>";
</script>
<!-- <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/home.js"></script> -->
