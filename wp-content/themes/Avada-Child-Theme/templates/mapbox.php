<?php
    $interactive = 0;
    $geoJSON = "home.json";
    if(is_page(230) or  is_page("authoritarian-interference-tracker")) {
        $interactive = 0;
        $geoJSON = "home.json";
    } else {
        $interactive = 1;
        $geoJSON = "threats.json";
    }
    

?>
<script type="text/javascript">
    function showMap(map,response) {
        map["dragPan"].enable();
        map["scrollZoom"].enable();
        map["boxZoom"].enable();
        // map["dragRotate"].enable();
        map["keyboard"].enable();
        map["doubleClickZoom"].enable();
        map["touchZoomRotate"].enable();
        
        jQuery("#site-hero").fadeOut("slow");
        jQuery(".threat-legend").fadeIn("slow");
        jQuery(".map-overlay").fadeToggle("slow");
        jQuery(".map__explore").addClass("fadeIn animated");
        var boundingBox = getBoundingBox(response);
        var padding = {};
        // set different bounding padding for desktop and mobile
        if (document.documentElement.clientWidth > 800) {
            padding = {
                top: 50,
                left: 175,
                bottom: 50,
                right: 175
            };
            
        }
        else {
            padding = {
                top: 0,
                left: 25,
                bottom: 0,
                right: 25
            }
        }

        map.fitBounds([[boundingBox.xMin, boundingBox.yMin], [boundingBox.xMax, boundingBox.yMax]],{
                padding : padding
            });
    }
    function getBoundingBox(data) {
        var bounds = {}, coords, point, latitude, longitude;

        for (var i = 0; i < data.features.length; i++) {
            coords = data.features[i].geometry.coordinates;
        
            longitude = coords[0];
            
            latitude = coords[1];
            // console.log("country: " + data.features[i].properties.country + " | long: " + longitude);
            bounds.xMin = bounds.xMin < longitude ? bounds.xMin : longitude;
            bounds.xMax = bounds.xMax > longitude ? bounds.xMax : longitude;
            bounds.yMin = bounds.yMin < latitude ? bounds.yMin : latitude;
            bounds.yMax = bounds.yMax > latitude ? bounds.yMax : latitude;
        
        }

        return bounds;
    }
</script>
<?php if(is_page(230) or  is_page("authoritarian-interference-tracker")) : get_template_part('templates/map_home'); ?>   
<?php else : get_template_part('templates/map_threats');?>
<?php endif; ?>


<script>
    function slugify(text)
    {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }
    // var map;

    function filterItem(item, query){
        return item.filter(function(el) {
            return el.indexOf(query) == -1;
        });
    }

    (function($) {
        $.fn.extend({
            animateCss: function(animationName, callback) {
                var animationEnd = (function(el) {
                var animations = {
                    animation: 'animationend',
                    OAnimation: 'oAnimationEnd',
                    MozAnimation: 'mozAnimationEnd',
                    WebkitAnimation: 'webkitAnimationEnd',
                };

                for (var t in animations) {
                    if (el.style[t] !== undefined) {
                    return animations[t];
                    }
                }
                })(document.createElement('div'));

                this.addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass( animationName);

                if (typeof callback === 'function') callback();
                });

                return this;
            },
        });
    })(jQuery);
</script>