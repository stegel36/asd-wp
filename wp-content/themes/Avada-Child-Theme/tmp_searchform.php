<?php
/**
 * The search-form template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<!-- <form role="search" class="searchform fusion-search-form" method="get" action="<?php echo esc_url_raw( home_url( '/' ) ); ?>"> -->
<div class="searchform fusion-search-form">
	<div class="fusion-search-form-content">
		<div class="fusion-search-field search-field">hi!
            <?php echo facetwp_display( 'facet', 'content' ); ?>
            <div style="display:none"><?php echo facetwp_display( 'template', 'Example' ); ?></div>
            <button class="fwp-submit" data-href="/search-results/">Submit</button>

		</div>
	</div>
</div>
