(function($) {
    $(document).on('facetwp-loaded', function() {
        
        $("#reset").click(function() {

            if ( window.location.href.indexOf("country/") > 0 ) {
                var country = FWP.facets.country;
                FWP.facets.keywords = "";
                FWP.facets.threat = "";
                FWP.facets.time = "";
                FWP.facets.date_slider = "";
                FWP.facets.country= country;
                
                FWP.set_hash();
                FWP.fetch_data();
            }
            else {
                
                FWP.reset();
            }
            
        })
        $("#search").click(function() {
    
            FWP.refresh();
        });
    });

	$(document).on('facetwp-refresh', function() {
		if (FWP.loaded) {
			$('.facetwp-template').prepend('<div class="loadingio-spinner-spinner-bkkx0fm8zpr"><div class="ldio-vxcvn8cifwd"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>');
		}
		$('.asd-result').animate({ opacity: 0 }, 1000);
	});

	$(document).on('facetwp-loaded', function() {
        	$('.asd-result').animate({ opacity: 1 }, 1000);
    	});
})(jQuery);
