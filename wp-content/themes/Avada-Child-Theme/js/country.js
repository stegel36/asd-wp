// Version: 1.0

(function($) {
    // store variable of whether we want to do a "to present" search
    var toPresent = false;
    var endValue = ""; 
    $(document).ready(function() {
        
        // setup offset value
        var offset = $(".threat-search").offset();
        var offsetTop = offset.top;
        if($("body").hasClass("admin-bar")) {
            offsetTop = offset.top + 32;
        }
        $(document).scroll(function() {
            
            // console.log($(window).scrollTop() + " > " + offsetTop);
            if ($(window).scrollTop() > 398) {
                $(".threat-search").addClass("threat-search--sticky");
            }
            else {
                $(".threat-search").removeClass("threat-search--sticky");
            }

        });

        // return to threats page;
        $("#returnToThreats").click(function(e) {
            var url = document.referrer;
            // url = "https://localhost/asd-wp/threats/";

            var baseURL = (document.hostname == "localhost") ? "https://localhost/asd-wp" : document.hostname;
            // https://localhost/asd-wp/country/united%20states/#3646 or https://asd.ajsiegel.com/country/united%20states/#3646
            if(url.indexOf("/threats") >= 0) {
                window.location.href = document.referrer;
            }
            else{
                
                window.location.href = baseURL + "/threats";
            }

            e.preventDefault();
        });

       
    });

    // set interesting text in title
    $(document).on('facetwp-loaded', function() {
        $("#timestamp").text("as of " + moment().format("MMM Do, YYYY"));
        $(".countryBanner__count").text(FWP.settings.pager.total_rows + " incidents");
        // FWP.facets.country = ["<?php echo $country; ?>"];
        
        FWP.load_from_hash();

        $(document).ready(function() {
            if(typeof FWP.facets.date_slider === "object" && FWP.facets.date_slider.length === 1) {
                console.log("no end date");
                toPresent = true;
                $("#toPresent").prop("checked",true);
                toggleRange($("#toPresent"));
                // var startDate = moment.unix($('#dateSlider').slider("values",0)).format('YYYYMMDD');
                // $('#dateSlider').slider("option","range",false);
                // $('#dateSlider #slide-right').hide();
                // $('#dateSlider').slider("values",1, "present");
                // $( '#amount' ).val(startDate);
                // $( '#amount' ).change();
               
            }
            else {
                toPresent = false;
            }

            $(document).on("change","#toPresent", function() {
                
            //     var startDate = moment.unix($('#dateSlider').slider("values",0)).format('YYYYMMDD');
            //     console.log("checked");
            //     if(this.checked) {
            //         toPresent = true;
            //         endValue = $('#dateSlider').slider("values", 1);
            //         $('#dateSlider').slider("option","range",false);
            //         $('#dateSlider #slide-right').hide();
            //         $('#dateSlider').slider("values",1, 0);
            //         $( '#amount' ).change();
            //         // update amount
                    
            //         $( '#amount' ).val(startDate);
                    
            //    }
            //    else {
            //        toPresent = false;
            //        $('#dateSlider').slider("option","range",true);
            //        $('#dateSlider').slider("values",1,endValue);
            //        $('#dateSlider #slide-right').show();
                
            //        var endDate = moment.unix($('#dateSlider').slider("values",1)).format('YYYYMMDD');
            //        $( '#amount' ).val(startDate + ',' + endDate);
            //        $( '#amount' ).change();
            //    }
                toggleRange($("#toPresent"));
        
            });
        });
        
        // FWP.set_hash();
        // FWP.fetch_data();
        // FWP.refresh();

       
    });

    // animate copy link button
    $(document).on("click",".share__button--copy", function(e) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($(this).attr("data-url")).select();
		document.execCommand("copy");
		$temp.remove();
		e.preventDefault();
		$(this).find(".share__text").text("Copied!");
		return false;
    });
    
    // show advanced search
    $(document).on("click","#advanced-search",function(e) {
		var icon = $(this).find(".fontawesome-icon");

		icon.toggleClass("fa-plus-square fa-minus-square");
		
		if(icon.hasClass("fa-minus-square"))
		{
			$(".advanced-search").addClass("advanced-search--shown");    
			$(".advanced-search").animate({
				"opacity" : 1,
				"height" : "90px"
			});
		}
		else{
			$(".advanced-search").animate({
				"opacity" : 0,
				"height" : "0px"
			},function(){
				$(this).removeClass("advanced-search--shown");
			});
		}
		return false;
    });
    
    // toggle the to present search
    // $(document).on("click","#toPresent",function() {
    //     if(this.checked) {
    //         toPresent = true;
    //         endValue =  $('#dateSlider').slider("values",1);
    //         toggleRange(false);
    //    }
    //    else {
    //        toPresent = false;
    //         toggleRange(true, endValue);
    //    }

    // });

    

    // deal with date slider when To Present is set
    $(document).on('facetwp-refresh', function() {
        if(toPresent) {
            
            // FWP.facets.date_slider = moment.unix($('#dateSlider').slider("values",0)).format('YYYYMMDD');
        }
        
    });

    var toggleRange = function(obj) {
        var startDate = moment.unix($('#dateSlider').slider("values",0)).format('YYYYMMDD');
        console.log("toggleRange");
        if(obj.attr("checked")) {
            toPresent = true;
            
            $('#dateSlider').slider("option","range",false);
            $('#dateSlider #slide-right').hide();
            $('#dateSlider').slider("values",1, 0);
            $( '#amount' ).change();
            // update amount
            
            $( '#amount' ).val(startDate);
            
       }
       else {
           toPresent = false;
           $('#dateSlider').slider("option","range",true);
           $('#dateSlider #slide-right').show();
        //    $('#dateSlider').slider("values",1,$('#dateSlider').slider("option","max"));
        
        $('#dateSlider').slider("values",1,1546318800);
           
        
           var endDate = moment.unix($('#dateSlider').slider("values",1)).format('YYYYMMDD');
           $( '#amount' ).val(startDate + ',' + endDate);
           $( '#amount' ).change();
       }
    }
})(jQuery);
