// Version: 2.0
var interactive = 0;
var hostname = (location.hostname === "localhost") ? "https://localhost:3000" : "https://securingdemocracy.gmfus.org:3000";
var apiPrefix = (location.hostname === "localhost") ? "/asd-wp/" : "/";
var jsonFile = "home-old.json";
(function($) {
    var map;
    var powerURL = encodeURI(apiPrefix + "authoritarian-interference-tracker/");

    $(document).ready(function() {
        mapboxgl.accessToken = 'pk.eyJ1Ijoic2VjdXJpbmdkZW1vY3JhY3kiLCJhIjoiY2prcHg4emtwMWk1azNsbW4wcWx3MmFmaiJ9.k_pXFPY3kUe_fm87JjkCpQ';

        // setup default excludeFilter
        var excludedThreats = ["!in", "threat"];

        var zoomLevel = (document.documentElement.clientWidth < 800) ? 0.2 : 1.4;

        // setup map
        map = new mapboxgl.Map({
            container: 'map', 
            // map v2
            style: "mapbox://styles/securingdemocracy/cjlrf0ets8ews2sphwdgax1xm?fresh=true",
            center: [-8.78, 15.4],
            zoom: zoomLevel,
            dragPan: interactive,
            scrollZoom:  interactive,
            boxZoom:  interactive,
            dragRotate:  interactive,
            doubleClickZoom:  interactive,
            touchZoomRotate:  interactive,
            keyboard:  interactive        
        });
        
        // change keyboard to pointer on mouse over
        map.on("mouseenter", 'ops', function(e) {
            map.getCanvas().style.cursor = 'pointer';
        });
        map.on('mouseleave', 'ops', function() {
            map.getCanvas().style.cursor = '';
        });

        
        //load layer of data
        map.on("load", function() {
            // console.log(stylesheetDir + "/" + jsonFile);
            $.getJSON(stylesheetDir + "/" + jsonFile, function(response) {
                map.addSource("operations", {
                    "type" : "geojson",
                    "data" : response 
                });

                map.addLayer({
                    "id" : "ops",
                    "type": "circle",
                    "source" : "operations",
                    "paint" : {
                        "circle-radius" : 10,
                        "circle-stroke-width" : 8,
                        "circle-stroke-color" : [
                            'match',
                            ['get', 'threat'],
                            'malign-finance', '#519462',
                            'political-and-social-subversion', '#643eb2',
                            'information-operations', '#FF6347',
                            'cyberattacks', '#5194c0',
                            "strategic-economic-coercion", "#ffdf47",
                            "multiple" , "#AB3969",
                            /* other */ '#fff'
                        ],
                        "circle-color" : [
                            'match',
                            ['get', 'threat'],
                            'malign-finance', '#519462',
                            'political-and-social-subversion', '#643eb2',
                            'information-operations', '#FF6347',
                            'cyberattacks', '#5194c0',
                            "strategic-economic-coercion", "#ffdf47",
                            "multiple" , "#AB3969",
                            /* other */ '#fff'
                        ],
                        "circle-stroke-opacity" : 0.5
                    }
                });

                // set cookies to hide info
                var guideKey = "dontShow";
                $("#whatsHappening").click(function() {
                    var cookie = document.cookie.match('(^|;) ?' + guideKey + '=([^;]*)(;|$)');
                    var dontShow = (cookie) ? cookie[2] : null;
                    if(dontShow != "true") {
                        $(".map-guide").addClass("fadeIn animated");
                        jQuery("#site-hero").fadeOut("slow");
                    }
                    else {
                        showMap(map, response);
                    }
                    
                });

                $(".map-guide__button").click(function() {
                    if($("#dontShow").is(':checked')) {
                        var expires = new Date();
                        expires.setTime(expires.getTime() + (180 * 24 * 60 * 60 * 1000));
                        document.cookie = "dontShow" + '=' + "true" + ';expires=' + expires.toUTCString();
                    }
                    $(".map-guide").addClass("fadeOut");
                    showMap(map, response);
                });

                $(".threat-legend__item").click(function(e) {
                    
                    // if turning off a filter
                    if($("#" + e.target.id).hasClass("threat-legend__item--active")) {
                        excludedThreats.push(e.target.id);
                        if(excludedThreats.length > 6) {
                            excludedThreats.push("multiple");
                        }
                        map.setFilter("ops",excludedThreats);

                        // hide all incidents with threat slug
                        response.features.forEach(function(el,i) {
                            var newProperties = [];
                            
                            el.properties.incidents.forEach(function(incident,j) {
                                if(incident.tools[0].slug === e.target.id) {
                                    incident.display = false;
                                    response.features[i].properties.incidents[j] = incident;    
                                }
                                
                            });                            
                            
                        });
                        map.getSource("operations").setData(response);
                    }
                    // turn on threat
                    else {
                        excludedThreats = filterItem(excludedThreats, e.target.id); // remove threat slug from setFilter array

                        if(excludedThreats.length == 2) { // if no items in array, clear all filters
                            map.setFilter("ops");
                        }
                        else { // if items in array keep them
                            map.setFilter("ops",excludedThreats);
                        }
                        
                        // turn on all incidents with threat slug
                        response.features.forEach(function(el,i) {
                            var newProperties = [];
                            
                            el.properties.incidents.forEach(function(incident,j) {
                                if(incident.tools[0].slug === e.target.id) {
                                    incident.display = true;
                                    response.features[i].properties.incidents[j] = incident;    
                                }
                                
                            }); 
                        });
                        map.getSource("operations").setData(response);                            
                    }

                    $("#" + e.target.id).toggleClass("threat-legend__item--active");
                });
            });            
        });

        map.on("click", "ops", function(e) {
            // get data from pin
            
            var coordinates = e.features[0].geometry.coordinates.slice();
            var country = e.features[0].properties.country;
            var country_slug = slugify(country);
            var apiPrefix = (location.hostname === "localhost") ? "/asd-wp/" : "/";
            // FORNOW: disable country url
            // var countryURL = encodeURI(apiPrefix + "country/" + country.replace("-"," ")).toLowerCase();
            // setup powerbi url
            
            
            // get incidents for item
            var incidents = JSON.parse(e.features[0].properties.incidents);
            var incidentCount = 0;
            var noIncidents = true;
            // start creating the html container
            
            
            var popup = new mapboxgl.Popup({
                className : "animated fadeIn"
            });

            incidentCount = incidents.length
            var html = "<div class='map-popup " + ((incidentCount === 1) ? "map-popup--short" : " ") + "'>";

            // create a card for reach incident
            incidents.forEach(function(el, i) {
                
                if (el.display) {
                    var relatedFilterObj = [{
                        selector : {
                            $schema : "http://powerbi.com/product/schema#visualSelector",
                            visualName : "2d254a87a5b298b98c53"
                        },
                        state : {
                            filters : [
                                {
                                    $schema : "http://powerbi.com/product/schema#basic",
                                    target : {
                                        table : "Countries",
                                        column : "Country",
                                    },
                                    operator : "=",
                                    values : [country]
                                }
                            ]
                        },
                    },
                    {
                        selector : {
                            $schema : "http://powerbi.com/product/schema#visualSelector",
                            visualName : "fee64d853d2c3e579085"
                        },
                        state : {
                            filters : [{                        
                                $schema : "http://powerbi.com/product/schema#basic",
                                target : {
                                    table : "Tools",
                                    column : "Tool",
                                },
                                operator : "=",
                                values : [el.tools[0].name]
                            }]
                        }
                    }
                    ];
                    var incidentFilterObj = [{
                        $schema : "http://powerbi.com/product/schema#basic",
                        target : {
                            table : "Authoritarian Interference Tracker",
                            column : "Incident Index",
                        },
                        operator : "=",
                        values : [Number(el.post_id)]
                    }];

                    var incidentURL = powerURL + "?filters=" +  encodeURIComponent(JSON.stringify(incidentFilterObj));
                    var relatedURL = powerURL + "?slicers=" +  encodeURIComponent(JSON.stringify(relatedFilterObj));

                    html += "<div id='incident-" + (i) + "' class='  incident " + ((i===0) ? "animated" : " ") + " incident--" + el.tools[0].slug + "'>";
                    
                    html += "<h3>" + el.tools[0].name + "</h3>";
                    html += "<h2 class='incident__title'><a href='" + incidentURL + "'>" + el.title + "</a></h2>"; 
                    html += "<p>" + country + " - " + el.date_text + "</p>";
                    html += "<div class='incident__actions'";
                    html += "><a href='" + incidentURL +  "'  class='btn btn--" + el.tools[0].slug + "'>Details</a>";
                    html += "<a class='map-popup__related' href='" + relatedURL + "'>View Related</a>"
                    html += "</div>";
                    html += "</div>";
                    
                    noIncidents = false;
                }

            });
            
            // if more than 1 incident, show arrows
            if(incidentCount > 1) {
                
                $(".map-popup").addClass("map-popup--multiple");
                html += "<div class='map-popup__arrows'>";
                html += "<a href='#' class='prevIncident'><i class='fa fa-angle-left'></i></a>";
                html += "<span id='incident__index'>1</span>/" + incidentCount;
                html += "<a href='#' class='nextIncident'><i class='fa fa-angle-right'></i></a>";
                html += "</div>";
            }
            
            if(noIncidents) {
                html += "<div class='incident animated'><h3>No incidents available</h3>";
                html += "<p>Based on your current threat filters, there are no matching incidents. Please reset your filters to see incidents.</p>";
                html += "<button class='btn btn--primary' id='closePopup'>Close</button></div>";
            }
            html += "</div>";
            
            
            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            // while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            //     coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            // }
            popup.setLngLat(coordinates)
            .setHTML(html)
            .addTo(map);

            $("#closePopup").click(function(){
                popup.remove();
            });

           
            // events when popup closes
            // popup.on('close', function(e) {
            //     $('.mapboxgl-popup').addClass("animated fadeOut");
            // });

            //event when popup opens
            // popup.on("open", function(e) {
            //     console.log("hey");
            //     $('.mapboxgl-popup').addClass("animated fadeIn");
            // });

            
              
        });

        
    });

    // create latest threats block
    var createLatestThreat = function() {
        // console.log("add threat",latestThreatID);
        if (typeof latestThreatID !== "undefined") {
            var incidentFilterObj = [{
                $schema : "http://powerbi.com/product/schema#basic",
                target : {
                    table : "Authoritarian Interference Tracker",
                    column : "Incident Index",
                },
                operator : "=",
                values : [parseInt(latestThreatID)]
            }];

            var incidentURL = powerURL + "?filters=" +  encodeURIComponent(JSON.stringify(incidentFilterObj));

            $(".incident-alert").removeClass("incident-alert--hide");
            $(".incident-alert__content").html("<p><a href='" + incidentURL + "'>" + latestThreatTitle + "</a></p>");
        }

    }

    createLatestThreat();

    $('.home-map').on('click','.prevIncident',function() {
        var activeIncident = ($(this).parent().siblings('.incident.animated').attr('id'));
        var activeId = activeIncident.split("-")[1];
        var nextId = activeId - 1;
        var incidentCount = $(this).parent().siblings('.incident').length;
        // go to last if activeId===0
        if(nextId < 0) {
            nextId = incidentCount - 1;
        }
        $("#incident__index").text(nextId + 1);  
        $("#" + activeIncident).animateCss("fadeOut fastest", function() {
            $("#" + activeIncident).toggleClass("animated");
            $("#incident-" + nextId).animateCss("fadeIn", function() {
                
            });
            
        });          
        
        return false;
    });

    $('.home-map').on('click','.nextIncident',function() {
        var activeIncident = ($(this).parent().siblings('.incident.animated').attr('id'));
        var activeId = parseInt(activeIncident.split("-")[1]);
        
        var nextId = activeId + 1;
        var incidentCount = $(this).parent().siblings('.incident').length;
        
        // go to last if activeId===0
        if(nextId === incidentCount) {
            nextId = 0;
            
        }
        $("#incident__index").text(nextId + 1);  
        $("#" + activeIncident).animateCss("fadeOut fastest", function() {
            $("#" + activeIncident).toggleClass("animated");
            $("#incident-" + nextId).animateCss("fadeIn", function() {
                
            });
            
        });
        return false;
    });

    $(".incident-alert__close").click(function() {
        $(".threat-legend").addClass('threat-legend--noAlert');
        $(".map-overlay").addClass("map-overlay--noAlert");
        $('.incident-alert').addClass('animated faster fadeOutUp').one(animationEnd,function() {
            // $(this).remove();
            
        });
        $('.map-overlay').toggleClass("map-overlay--alert");
    });

    var animationEnd = (function(el) {
        var animations = {
            animation: 'animationend',
            OAnimation: 'oAnimationEnd',
            MozAnimation: 'mozAnimationEnd',
            WebkitAnimation: 'webkitAnimationEnd',
        };

        for (var t in animations) {
            if (el.style[t] !== undefined) {
            return animations[t];
            }
        }
    })(document.createElement('div'));
    
})(jQuery);
