http_path = "." #root level target path
css_dir = "." #targets our default style.css file at the root level of our theme
sass_dir = "sass" #targets our sass directory
images_dir = "images" #targets our pre existing image directory
javascripts_dir = "js" #targets our JavaScript directory
 
# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
 
# To enable relative paths to assets via compass helper functions.
# note: this is important in wordpress themes for sprites
 
relative_assets = true
if environment == :development
    line_comments = true
    output_style = :nested
    sass_options = {:debug_info => true}
    sourcemap = true
end

if environment == :production
    line_comments = false
    output_style = :compressed
    sass_options = {:debug_info => false}
    sourcemap = true

    require 'fileutils'
        on_stylesheet_saved do |file|
            if File.exists?(file)
            filename = File.basename(file, File.extname(file))
            
            FileUtils.cp(file,  filename + ".min" + File.extname(file))
        end
    end
end 
