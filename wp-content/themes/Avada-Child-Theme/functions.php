<?php
// require_once( 'inc/httpful.phar');

function theme_enqueue_styles() {
	wp_register_script('mapbox', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js','',false,true);
	// wp_register_script('mapbox',get_stylesheet_directory_uri() . "/js/vendor/mapbox-gl.js","",false,true);
	wp_enqueue_script('mapbox');
	wp_register_script('moment', get_stylesheet_directory_uri() . "/js/moment.min.js",'',false,true);
	wp_enqueue_script('moment');
	wp_enqueue_style('mapbox-css', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css');
	wp_enqueue_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css');
	wp_enqueue_style('typekit-fonts', 'https://use.typekit.net/zzq2bbm.css');
	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/style.css');
	if(is_page("home-no-map")) {
		wp_register_script('home-js', get_stylesheet_directory_uri() . "/js/home.js",'',filemtime(plugin_dir_path( __FILE__ ) . 'js/home.js'),true);
		wp_enqueue_script("home-js");
	}
	//todo change this
	if(is_page("new-authoritarian-interference-tracker")) {
		wp_register_script('home-js', get_stylesheet_directory_uri() . "/js/incident-map.js",'',filemtime(plugin_dir_path( __FILE__ ) . 'js/incident-map.js'),true);
		wp_enqueue_script("home-js");
	}
	if(is_page("country")) {
		wp_register_script('share-button', get_stylesheet_directory_uri() . "/js/share-button.js",'',false,true);
		wp_enqueue_script("share-button");
		wp_register_script('country', get_stylesheet_directory_uri() . "/js/country.js",'',filemtime(plugin_dir_path( __FILE__ ) . 'js/country.js'),true);
		wp_enqueue_script("country");
	}
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'style-css' ),
        filemtime(plugin_dir_path( __FILE__ ) . '/style.css')
    );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

// *********************************
// AVADA CUSTOMIZATIONS
// *********************************

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );

// add_action('avada_header', 'avada_add_map' );

// function avada_add_map() {
// 	get_template_part( 'templates/mapbox');
// }

// add_action('avada_after_header_wrapper', 'avada_add_hero');

// function avada_add_hero() {
// 	get_template_part( 'templates/hero');
// }

// *********************************
// REST CUSTOMIZATIONS
// *********************************

// set rest endpoint for incidents
function incidents_rest_get( $data ) {
	$wp_query = array(
		'post_type' => 'asd_incidents',
	    'posts_per_page' => -1
	);

	$metaQuery = array();

	if(strlen($data['startedAfter']) > 0 ) {
		$metaQuery[] = array(
			"key" => "start_date",
			"value" => $data['startedAfter'],
			"compare" => ">"
		);

	}

	$wp_query['meta_query'] = $metaQuery;
	$incidents = new WP_Query($wp_query);

	
	

// here is where you can loop through each post and add any of the custom fields. ie. below, 'location type' is one of my ACF fields
	foreach ($incidents->posts as $key => $post) {
			$incidents->posts[ $key ]->longitude = get_post_meta( $post->ID, 'longitude', true );
			$incidents->posts[ $key ]->latitude = get_post_meta( $post->ID, 'latitude', true );
			$incidents->posts[ $key ]->country = get_post_meta( $post->ID, 'country', true );
			$incidents->posts[ $key ]->tools = get_the_terms( $post->ID, "asd_tools");
			$incidents->posts[ $key ]->tools = get_the_terms( $post->ID, "asd_threat_actor");
		//added 1117
			$incidents->posts[ $key ]->featured = get_post_meta( $post->ID, 'featured', true );	
			$incidents->posts[ $key ]->start_date = get_post_meta( $post->ID, 'start_date', true );		
			$incidents->posts[ $key ]->end_date = get_post_meta( $post->ID, 'end_date', true );	
		}
	return $incidents->posts;
}

/* Custom REST endpoint for incidents */
// register the route
add_action( 'rest_api_init', function () {
	register_rest_route( '/incidents/v1', '/all', array(
		'methods' => 'GET',
		'callback' => 'incidents_rest_get',
	) );
} );
add_action( 'rest_api_init', function () {
	register_rest_route( '/incidents/v1', '/all/startedAfter=(?P<startedAfter>[0-9-]+)', array(
		'methods' => 'GET',
		'callback' => 'incidents_rest_get',
	) );
} );

// creat rest endpoint for incidents by threat and country
function incidents_rest_get_threat_country( $data) {
	$wp_query = array(
		'post_type' => 'asd_incidents',
		'posts_per_page' => -1
		
	);

	
	if((strlen($data['threat']) > 0) && ($data['threat'] != "any")){
		
		$wp_query['tax_query'] = array(
			array(
				"taxonomy" => "asd_tools",
				"field" => (preg_match('/[A-Za-z]+/',$data['threat'])) ? "slug" : "id",
				"terms" => $data['threat']
			)
			);
	}
	// print_r($wp_query);
	$metaQuery = array();
	if((strlen($data['country']) > 0 ) && ($data['country'] != "any")){
		$value = explode(",",urldecode(str_replace("-"," ",$data['country'])));
		// echo $value;
		$metaQuery[] = array(
			"key" => "country",
			"value" => $value,
			"compare" => "IN"
		);

	}
	if((strlen($data['startedAfter']) > 0 ) && ($data['startedAfter'] != "any")) {
		$dateArray = explode(",",$data['startedAfter']);

		$metaQuery[] = array(
			"key" => "start_date",
			"value" => $dateArray[0],
			"compare" => ">="
		);
		if(sizeof($dateArray) > 1)
		{
			$metaQuery[] = array(
				"key" => "end_date",
				"value" => $dateArray[1],
				"compare" => "<="
			);
		}

	}

	if(strlen($data['keywords']) > 0) {
		$wp_query['s'] = $data['keywords'];
	}
	if(strlen($data['search']) > 0) {
		$wp_query['s'] = $data['search'];
	}
	

	$wp_query['meta_query'] = $metaQuery;
	// print_r($wp_query);
	$incidents = new WP_Query($wp_query);
// here is where you can loop through each post and add any of the custom fields. ie. below, 'location type' is one of my ACF fields
	foreach ($incidents->posts as $key => $post) {
			$incidents->posts[ $key ]->longitude = get_post_meta( $post->ID, 'longitude', true );
			$incidents->posts[ $key ]->latitude = get_post_meta( $post->ID, 'latitude', true );
			$incidents->posts[ $key ]->country = get_post_meta( $post->ID, 'country', true );
			$incidents->posts[ $key ]->tools = get_the_terms( $post->ID, "asd_tools");
			$incidents->posts[ $key ]->tools = get_the_terms( $post->ID, "asd_threat_actor");
				//added 1117
			$incidents->posts[ $key ]->featured = get_post_meta( $post->ID, 'featured', true );
			$incidents->posts[ $key ]->date_text = get_post_meta( $post->ID, 'date_text', true );
			$incidents->posts[ $key ]->start_date = get_post_meta( $post->ID, 'start_date', true );		
			$incidents->posts[ $key ]->end_date = get_post_meta( $post->ID, 'end_date', true );		
			$incidents->posts[ $key ]->startDate = $dateArray[0];
	}
	return $incidents->posts;
}
/* Custom REST endpoint for incidents */
// register the route
add_action( 'rest_api_init', function () {
	register_rest_route( '/incidents/v1', '/threatCountry/(?P<threat>[a-zA-Z0-9-, ]+)/(?P<country>[a-zA-Z0-9-, %]+)/(?P<startedAfter>[a-zA-Z0-9-, %]+)', array(
		'methods' => 'GET',
		'callback' => 'incidents_rest_get_threat_country',
	) );
} );
add_action( 'rest_api_init', function () {
	register_rest_route( '/incidents/v1', '/threatCountry/country=(?P<country>[a-zA-Z0-9-,]+)/threat=(?P<threat>[a-zA-Z0-9-,]+)/startedAfter=(?P<startedAfter>[a-zA-Z0-9-,]+)', array(
		'methods' => 'GET',
		'callback' => 'incidents_rest_get_threat_country',
	) );
} );

add_action( 'rest_api_init', function () {
	register_rest_route( '/incidents/v1', '/threatCountry/country=(?P<country>[a-zA-Z0-9-,]+)/threat=(?P<threat>[a-zA-Z0-9-,]+)/startedAfter=(?P<startedAfter>[a-zA-Z0-9-,]+)/keywords=(?P<keywords>[a-zA-Z0-9-,]+)', array(
		'methods' => 'GET',
		'callback' => 'incidents_rest_get_threat_country',
	) );
} );
add_action( 'rest_api_init', function () {
	register_rest_route( '/incidents/v1', '/threatCountry/country=(?P<country>[a-zA-Z0-9-,]+)/threat=(?P<threat>[a-zA-Z0-9-,]+)/keywords=(?P<keywords>[a-zA-Z0-9-,]+)', array(
		'methods' => 'GET',
		'callback' => 'incidents_rest_get_threat_country',
	) );
} );

add_action( 'rest_api_init', function () {
	register_rest_route( '/incidents/v1', '/threatCountry/country=(?P<country>[a-zA-Z0-9-, %]+)/threat=(?P<threat>[a-zA-Z0-9-,]+)', array(
		'methods' => 'GET',
		'callback' => 'incidents_rest_get_threat_country',
	) );
} );

/* Custom REST endpoint for Homepage Feature */
// register the route
add_action( 'rest_api_init', function () {
	register_rest_route( '/featured/v1', '/all/(?P<count>\d+)', array(
		'methods' => 'GET',
		'callback' => 'featured_rest_get',
	) );
} );
// set rest endpoint for incidents
function featured_rest_get( $data ) {
	
	$featured = new WP_Query([
		'order' => 'DESC',
		'orderBy' => 'publish_date',
		// 'post_type' => 'any',
		'post_type' => 'asd_incidents',
	    // 'tag' => 'featured',
	    'posts_per_page' => $data->get_param('count')
		]);
// here is where you can loop through each post and add any of the custom fields. ie. below, 'location type' is one of my ACF fields
	foreach ($featured->posts as $key => $post) {
			$featured->posts[ $key ]->longitude = get_post_meta( $post->ID, 'longitude', true );
			$featured->posts[ $key ]->latitude = get_post_meta( $post->ID, 'latitude', true );		
			$featured->posts[ $key ]->country = get_post_meta( $post->ID, 'country', true );			
			$featured->posts[ $key ]->date_text = get_post_meta( $post->ID, 'date_text', true );		
			$featured->posts[ $key ]->tools = get_the_terms( $post->ID, "asd_tools");
			$featured->posts[ $key ]->threat_actors = get_the_terms( $post->ID, "asd_threat_actor");
			$featured->posts[ $key ]->campaigns = get_the_terms( $post->ID, "campaign");
			$featured->posts[ $key ]->start_date = get_post_meta( $post->ID, "start_date");
			$featured->posts[ $key ]->end_date = get_post_meta( $post->ID, "end_date");
			$featured->posts[ $key ]->post_link = get_permalink($post->ID);
			$featured->posts[ $key ]->incident_index = get_post_meta( $post->ID, 'incident_index', true );		
		}
	return $featured->posts;
}


// *********************************
// ACF CUSTOMIZATIONS
// *********************************

/* Register ACF Google Map KEy */
function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyC3IOtOKh19jERfx2ujjX9bHKAKxAVQNnI');
}
add_action('acf/init', 'my_acf_init');

function show_acf_rest_admin() {
	add_filter( 'acf/rest_api/field_settings/show_in_rest', '__return_true' );
}
add_action('acf/init', 'show_acf_rest_admin');


function handle_error() {
	$email = "<a href='mailto:nick.nigro@atlaspolicy.com?subject=Error Code 001'>support</a>";
	if ( array_key_exists( 'asd-save-post-error', $_GET) ) { ?>
	    <div class="error">
		<p>
		    <?php
			switch($_GET['asd-save-post-error']) {
			    case '001':
				echo "Unable to fetch data for post. Please contact $email :with error code 101";
				break;
			    case 'another_error_code':
				echo 'The post failed to save because reasons.';
				break;
			    default:
				echo 'An error ocurred when saving the post.';
				break;
			}
		    ?>
		</p>
	    </div><?php
	}
}
// add a hook for admin notices
add_action( 'admin_notices', 'handle_error' );

/* Store Country after saving with google map */
function acf_store_country_after_after_save(){
	$error = false;
	 
	// bail early if no ACF data
    if( empty($_POST['acf'])) {
        echo "empty";die();
        return;    
    }
	
		$location =  get_field('location');
		
		$country = $location['address'];
		$long = $location['lng'];
		$lat = $location['lat'];

				//echo $country;
		//echo $long;
		//echo $lat;

		update_field("Country",$country,$postID);

		update_field("Longitude",  $long,$postID);
		update_field("Latitude", $lat,$postID);

		$africa = array("Algeria", "Angola", "Benin", "Botswana", "Burkina", "Burundi", "Cameroon", "Cape Verde", "Central African Republic", "Chad", "Comoros", "Congo", "Congo, Democratic Republic of", "Djibouti", "Egypt", "Equatorial Guinea", "Eritrea", "Ethiopia", "Gabon", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Ivory Coast", "Kenya", "Lesotho", "Liberia", "Libya", "Madagascar", "Malawi", "Mali", "Mauritania", "Mauritius", "Morocco", "Mozambique", "Namibia", "Niger", "Nigeria", "Rwanda", "Sao Tome and Principe", "Senegal", "Seychelles", "Sierra Leone", "Somalia", "South Africa", "South Sudan", "Sudan", "Swaziland", "Tanzania", "Togo", "Tunisia", "Uganda", "Zambia", "Zimbabwe");
					
		$nAmerica = array("Antigua and Barbuda", "Bahamas", "Barbados", "Belize", "Canada", "Costa Rica", "Cuba", "Dominica", "Dominican Republic", "El Salvador", "Grenada", "Guatemala", "Haiti", "Honduras", "Jamaica", "Mexico", "Nicaragua", "Panama", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Trinidad and Tobago", "United States");
		$sAmerica = array("Argentina", "Bolivia", "Brazil", "Chile", "Colombia", "Ecuador", "Guyana", "Paraguay", "Peru", "Suriname", "Uruguay", "Venezuela");
		$europe = array("Albania", "Andorra", "Armenia", "Austria", "Azerbaijan", "Belarus", "Belgium", "Bosnia and Herzegovina", "Bulgaria", "Croatia", "Cyprus", "CZ", "Denmark", "Estonia", "Finland", "France", "Georgia", "Germany", "Greece", "Hungary", "Iceland", "Ireland", "Italy", "Latvia", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Malta", "Moldova", "Monaco", "Montenegro", "Netherlands", "Norway", "Poland", "Portugal", "Romania", "San Marino", "Serbia", "Slovakia", "Slovenia", "Spain", "Sweden", "Switzerland", "Ukraine", "United Kingdom", "Vatican City");
		$oceania = array("Australia", "Fiji", "Kiribati", "Marshall Islands", "Micronesia", "Nauru", "New Zealand", "Palau", "Papua New Guinea", "Samoa", "Solomon Islands", "Tonga", "Tuvalu", "Vanuatu");
		$Asia = array("Afghanistan", "Bahrain", "Bangladesh", "Bhutan", "Brunei", "Burma (Myanmar)", "Cambodia", "China", "East Timor", "India", "Indonesia", "Iran", "Iraq", "Israel", "Japan", "Jordan", "Kazakhstan", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Lebanon", "Malaysia", "Maldives", "Mongolia", "Nepal", "Oman", "Pakistan", "Philippines", "Qatar", "Russia", "Saudi Arabia", "Singapore", "Sri Lanka", "Syria", "Tajikistan", "Thailand", "Turkey", "Turkmenistan", "United Arab Emirates", "Uzbekistan", "Vietnam", "Yemen");
					
		if (in_array($country, $africa)) {
			update_field("Region","Africa",$postID);
		}if (in_array($country, $nAmerica)) {
			update_field("Region","N. America",$postID);
		}if (in_array($country, $sAmerica)) {
			update_field("Region","S. America",$postID);
		}if (in_array($country, $europe)) {
			update_field("Region","Europe",$postID);
		}if (in_array($country, $oceania)) {
			update_field("Region","Oceania",$postID);
		}if (in_array($country, $Asia)) {
			update_field("Region","Asia",$postID);
		}
    

}

add_action('acf/save_post', 'acf_store_country_after_after_save', 20);

// *********************************
// FUSION CUSTOMIZATIONS
// *********************************

 if ( ! function_exists( 'fusion_render_post_metadata' ) ) {
	/**
	 * Render the full meta data for blog archive and single layouts.
	 *
	 * @param string $layout    The blog layout (either single, standard, alternate or grid_timeline).
	 * @param string $settings HTML markup to display the date and post format box.
	 * @return  string
	 */
	function fusion_render_post_metadata( $layout, $settings = array() ) {

		$html     = '';
		$author   = '';
		$date     = '';
		$metadata = '';

		$settings = ( is_array( $settings ) ) ? $settings : array();

		$default_settings = array(
			'post_meta'          => fusion_library()->get_option( 'post_meta' ),
			'post_meta_author'   => fusion_library()->get_option( 'post_meta_author' ),
			'post_meta_date'     => fusion_library()->get_option( 'post_meta_date' ),
			'post_meta_cats'     => fusion_library()->get_option( 'post_meta_cats' ),
			'post_meta_tags'     => fusion_library()->get_option( 'post_meta_tags' ),
			'post_meta_comments' => fusion_library()->get_option( 'post_meta_comments' ),
		);

		$settings = wp_parse_args( $settings, $default_settings );
		
		$post_meta = get_post_meta( get_queried_object_id(), 'pyre_post_meta', true );
		
		// Check if meta data is enabled.
		if ( ( $settings['post_meta'] && 'no' !== $post_meta ) || ( ! $settings['post_meta'] && 'yes' === $post_meta ) ) {

			// For alternate, grid and timeline layouts return empty single-line-meta if all meta data for that position is disabled.
			if ( in_array( $layout, array( 'alternate', 'grid_timeline' ), true ) && ! $settings['post_meta_author'] && ! $settings['post_meta_date'] && ! $settings['post_meta_cats'] && ! $settings['post_meta_tags'] && ! $settings['post_meta_comments'] ) {
				return '';
			}

			// Render the updated meta data or at least the rich snippet if enabled.
			if ( $settings['post_meta_date'] ) {
				$metadata .= fusion_render_rich_snippets_for_pages( false, false, true );

				$formatted_date = get_the_time( fusion_library()->get_option( 'date_format' ) );

				if(is_single()) {
					$date_markup = '<div class="asd-meta__date asd-meta__date--single">' . $formatted_date . '</div>';
				}
				else {
					$date_markup = '<span class="asd-meta__date">' . $formatted_date . '</span><span class="fusion-inline-sep">|</span>';
				}
				
				$metadata .= apply_filters( 'fusion_post_metadata_date', $date_markup, $formatted_date );
			} else {
				$date .= fusion_render_rich_snippets_for_pages( false, false, true );
			}
			// Render author meta data.
			if ( $settings['post_meta_author'] ) {
				ob_start();
				if(function_exists("coauthors_posts_links")) {
					coauthors_posts_links();
				}
				else {
					the_author_posts_link();
				}
				
				$author_post_link = ob_get_clean();
				$author_id = get_the_author_meta('ID');
				// Check if rich snippets are enabled.
				if ( fusion_library()->get_option( 'disable_date_rich_snippet_pages' ) && fusion_library()->get_option( 'disable_rich_snippet_author' ) ) {
					/* translators: The author. */
					
					if(is_single()) {
						
						if(function_exists("get_coauthors")) {
							$coauthors = get_coauthors(get_queried_object_id());
							if(is_array($coauthors) ) {
								$metadata .= '<div class="asd-authors">';
								foreach($coauthors as $theAuthor) {
									$metadata .= "<div class='asd-author'>";
									$metadata .= "<div class='asd-author__avatar'><img class='asd-author__image' src='" . get_field("profile_photo","user_" . $theAuthor->ID) . "' /></div>";
									$metadata .= '<div class="asd-author__meta"><div><span class="vcard"><span class="fn asd-author__name">' . coauthors_posts_links_single($theAuthor) . '</span></span></div>';
									$metadata .= '<div class="asd-author__title">' . get_field("job_title", "user_" . $theAuthor->ID) . "</div></div>";
									$metadata .= "</div>";
								}
								$metadata .= "</div>";
							}
						} else{ 
							$metadata .= "<div class='asd-author'>";
							$metadata .= "<div class='asd-author__avatar'><img class='asd-author__image' src='" . get_field("profile_photo","user_" . $author_id) . "' /></div>";
							$metadata .= '<div class="asd-author__meta"><div><span class="vcard"><span class="fn asd-author__name">' . $author_post_link . '</span></span></div>';
							$metadata .= '<div class="asd-author__title">' . get_field("job_title", "user_" . $author_id) . "</div></div>";
							$metadata .= "</div>";

						}
					} else {
						
						$metadata .= sprintf( esc_attr__( 'By %s', 'Avada' ), '<span class="vcard"><span class="fn">' . $author_post_link . '</span></span>' );
						
					}
					
				} else {
					/* translators: The author. */
					$metadata .= sprintf( esc_attr__( 'By %s', 'Avada' ), '<span>' . $author_post_link . '</span>' );
				}
				

				$metadata .= '<span class="fusion-inline-sep">|</span>';
			} else { // If author meta data won't be visible, render just the invisible author rich snippet.
				$author .= fusion_render_rich_snippets_for_pages( false, true, false );
			}

			

			// Render rest of meta data.
			// Render categories.
			if ( $settings['post_meta_cats'] ) {
				ob_start();
				the_category( ', ' );
				$categories = ob_get_clean();

				if ( $categories ) {
					/* translators: The categories list. */
					$metadata .= ( $settings['post_meta_tags'] ) ? sprintf( esc_html__( 'Categories: %s', 'Avada' ), $categories ) : $categories;
					$metadata .= '<span class="fusion-inline-sep">|</span>';
				}
			}

			// Render tags.
			if ( $settings['post_meta_tags'] ) {
				ob_start();
				the_tags( '' );
				$tags = ob_get_clean();

				if ( $tags ) {
					/* translators: The tags list. */
					$metadata .= '<span class="meta-tags">' . sprintf( esc_html__( 'Tags: %s', 'Avada' ), $tags ) . '</span><span class="fusion-inline-sep">|</span>';
				}
			}

			// Render comments.
			if ( $settings['post_meta_comments'] && 'grid_timeline' !== $layout ) {
				ob_start();
				comments_popup_link( esc_html__( '0 Comments', 'Avada' ), esc_html__( '1 Comment', 'Avada' ), esc_html__( '% Comments', 'Avada' ) );
				$comments = ob_get_clean();
				$metadata .= '<span class="fusion-comments">' . $comments . '</span>';
			}

			// Render the HTML wrappers for the different layouts.
			if ( $metadata ) {
				$metadata = $author . $date . $metadata;

				if ( 'single' === $layout ) {
					$html .= '<div class="fusion-meta-info fusion-meta-info--single"><div class="fusion-meta-info-wrapper">' . $metadata . '</div></div>';
				} elseif ( in_array( $layout, array( 'alternate', 'grid_timeline' ), true ) ) {
					$html .= '<p class="fusion-single-line-meta">' . $metadata . '</p>';
				} else {
					$html .= '<div class="fusion-alignleft">' . $metadata . '</div>';
				}
			} else {
				$html .= $author . $date;
			}
		} else {
			// Render author and updated rich snippets for grid and timeline layouts.
			if ( fusion_library()->get_option( 'disable_date_rich_snippet_pages' ) ) {
				$html .= fusion_render_rich_snippets_for_pages( false );
			}
		}// End if().

		return apply_filters( 'fusion_post_metadata_markup', $html );
	}
}// End if().

// *********************************
// FACETWP CUSTOMIZATIONS
// *********************************

// add facetwp basic actions
add_action('wp_footer','add_facetwp_actions');
function add_facetwp_actions(){
	wp_register_script('facet', get_stylesheet_directory_uri() . "/js/facet.js",'',filemtime(plugin_dir_path( __FILE__ ) . "js/facet.js"),true);
	// echo "hello: " .afilemtime(plugin_dir_path( __FILE__ ) . "js/facet.js");
	wp_enqueue_script('facet');
}

// check fo facet query as main
function facetwp_is_main_query( $is_main_query, $query ) {
    if ( isset( $query->query_vars['facetwp'] ) ) {
        $is_main_query = true;
    }
    return $is_main_query;
}
add_filter( 'facetwp_is_main_query', 'facetwp_is_main_query', 10, 2 );

// check who can have access to manage facetwp
function fwp_api_check_permissions() {
    return current_user_can( 'manage_options' );
}
add_filter( 'facetwp_api_can_access', 'fwp_api_check_permissions' );

// disable autorefresh of FacetWP search
function fwp_disable_auto_refresh() {
	global $post;
$direct_parent = $post->post_parent;

	//
	if(is_page("threats") || is_page("country") || is_page("search-results") || is_page("new-authoritarian-interference-tracker") || is_page("test-tracker") || is_archive()) :
	?>
	<script>
	(function($) {
		$(function() {
			FWP.auto_refresh = false;
		});
		$(document).ready(function($) {
     	FWP.refresh();
   		});

		//force refresh on enter press
		$(document).keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            FWP.parse_facets();
            FWP.set_hash();
            $( ".apply-button" ).trigger( "click" ); 
        }
// 		$.noConflict();

    });
	})(jQuery);
	</script>
<?php
	endif;
}
add_action( 'wp_head', 'fwp_disable_auto_refresh', 100 );

// include coauthors in facetwp
add_filter( 'facetwp_facet_sources', function( $sources ) {
    if ( class_exists( 'coauthors_plus') && isset( $sources['taxonomies']['choices']['tax/author'] ) ) {
        $sources['co_authors'] = array(
            'label' => 'Co-Authors',
            'choices' => array(
                'tax/author' => 'Authors'
            )
        );
    }
    return $sources;
}, 10 );

// Replace Label with Display Name in facetwp authors
add_filter( 'facetwp_index_row', function( $params, $class ) {
	if ( 'authors' == $params['facet_name'] ) {
	$raw_value = $params['facet_value']; // cap-nicename
	$nicename = substr($raw_value, 4); // nicename
	$coauthor = get_user_by('slug',$nicename); // get user data
	$params['facet_display_value'] = $coauthor->display_name; // Display Name
	}

	if ( 'date_slider' == $params['facet_name'] ) {
        $values = maybe_unserialize( $params['facet_value'] );
       
		$params['facet_display_value'] = date("Y-m-d",strtotime(get_post_meta($params['post_id'],"end_date",true)));
	
       
    }
	return $params;
	}, 10, 2 );
	
// *********************************
// GENERIC CUSTOMIZATIONS
// *********************************

// add smaller square image size
if ( function_exists( 'add_theme_support' ) ) { 
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'square-small', 150, 150, true); // name, width, height, crop 
    add_filter('image_size_names_choose', 'my_image_sizes');
}

function my_image_sizes($sizes) {
    $addsizes = array(
        "square-small" => __( "Small square image")
    );
    $newsizes = array_merge($sizes, $addsizes);
    return $newsizes;
}

// open external links in new window
add_action( 'wp_footer', 'awts_openlinks_innnewtab' ); // Write our JS below here
function awts_openlinks_innnewtab() { ?>

	<script type='text/javascript'>
	//<![CDATA[
	//Open External Links in New Window - Script by STCnetwork.org
	jQuery(document).ready(function(){
		jQuery("#content a[href^=http], #content a[href^=https]")
		.each(function(){
			if (this.href.indexOf(location.hostname) == -1){
				jQuery(this).attr({ "target":"_blank" })}
			if (this.href.indexOf(location.hostname) != -1){
				// if ( jQuery(this).attr('target') == '_blank') {jQuery(this).attr("target", "");}
			}
		})
	;});
	//]]>
	</script>
<?php
}


/**
 * Get site url for links 
 *
 * @author WPSnacks.com
 * @link http://www.wpsnacks.com
 */
function url_shortcode() {
return get_bloginfo('url');
}
add_shortcode('url','url_shortcode');

// Add Roles
// npnigro
// Add Team Role
function asd_register_roles() {
	add_role( 'team', __(
	'Team' ),
	array( ) );
	
	// Add Director Role
	add_role( 'director', __(
		'Director' ),
		array( ) );

	// Add Non-Resident Experts Role
	add_role( 'expert', __(
		'Expert' ),
		array( ) );
	
	if( get_role('experts') ){
		remove_role( 'experts' );
	}
}



//add_action('admin_init','update_incidents');
function update_incidents(){


	$posts = get_posts([
  	'post_type' => 'asd_incidents',
  	'post_status' => 'publish',
  	'numberposts' => -1
	]);
	

    foreach ($posts as $single_post){
		$postID  = $single_post->ID;
		$location =  get_field('location',$postID);
		
		$country = $location['address'];
		$long = $location['lng'];
		$lat = $location['lat'];

				//echo $country;
		//echo $long;
		//echo $lat;

		update_field("Country",$country,$postID);

		update_field("Longitude",  $long,$postID);
		update_field("Latitude", $lat,$postID);

		$africa = array("Algeria", "Angola", "Benin", "Botswana", "Burkina", "Burundi", "Cameroon", "Cape Verde", "Central African Republic", "Chad", "Comoros", "Congo", "Congo, Democratic Republic of", "Djibouti", "Egypt", "Equatorial Guinea", "Eritrea", "Ethiopia", "Gabon", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Ivory Coast", "Kenya", "Lesotho", "Liberia", "Libya", "Madagascar", "Malawi", "Mali", "Mauritania", "Mauritius", "Morocco", "Mozambique", "Namibia", "Niger", "Nigeria", "Rwanda", "Sao Tome and Principe", "Senegal", "Seychelles", "Sierra Leone", "Somalia", "South Africa", "South Sudan", "Sudan", "Swaziland", "Tanzania", "Togo", "Tunisia", "Uganda", "Zambia", "Zimbabwe");
					
		$nAmerica = array("Antigua and Barbuda", "Bahamas", "Barbados", "Belize", "Canada", "Costa Rica", "Cuba", "Dominica", "Dominican Republic", "El Salvador", "Grenada", "Guatemala", "Haiti", "Honduras", "Jamaica", "Mexico", "Nicaragua", "Panama", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Trinidad and Tobago", "United States");
		$sAmerica = array("Argentina", "Bolivia", "Brazil", "Chile", "Colombia", "Ecuador", "Guyana", "Paraguay", "Peru", "Suriname", "Uruguay", "Venezuela");
		$europe = array("Albania", "Andorra", "Armenia", "Austria", "Azerbaijan", "Belarus", "Belgium", "Bosnia and Herzegovina", "Bulgaria", "Croatia", "Cyprus", "CZ", "Denmark", "Estonia", "Finland", "France", "Georgia", "Germany", "Greece", "Hungary", "Iceland", "Ireland", "Italy", "Latvia", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Malta", "Moldova", "Monaco", "Montenegro", "Netherlands", "Norway", "Poland", "Portugal", "Romania", "San Marino", "Serbia", "Slovakia", "Slovenia", "Spain", "Sweden", "Switzerland", "Ukraine", "United Kingdom", "Vatican City");
		$oceania = array("Australia", "Fiji", "Kiribati", "Marshall Islands", "Micronesia", "Nauru", "New Zealand", "Palau", "Papua New Guinea", "Samoa", "Solomon Islands", "Tonga", "Tuvalu", "Vanuatu");
		$Asia = array("Afghanistan", "Bahrain", "Bangladesh", "Bhutan", "Brunei", "Burma (Myanmar)", "Cambodia", "China", "East Timor", "India", "Indonesia", "Iran", "Iraq", "Israel", "Japan", "Jordan", "Kazakhstan", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Lebanon", "Malaysia", "Maldives", "Mongolia", "Nepal", "Oman", "Pakistan", "Philippines", "Qatar", "Russia", "Saudi Arabia", "Singapore", "Sri Lanka", "Syria", "Tajikistan", "Thailand", "Turkey", "Turkmenistan", "United Arab Emirates", "Uzbekistan", "Vietnam", "Yemen");
					
		if (in_array($country, $africa)) {
			update_field("Region","Africa",$postID);
		}if (in_array($country, $nAmerica)) {
			update_field("Region","N. America",$postID);
		}if (in_array($country, $sAmerica)) {
			update_field("Region","S. America",$postID);
		}if (in_array($country, $europe)) {
			update_field("Region","Europe",$postID);
		}if (in_array($country, $oceania)) {
			update_field("Region","Oceania",$postID);
		}if (in_array($country, $Asia)) {
			update_field("Region","Asia",$postID);
		}
    }
}


add_action( 'init', 'asd_register_roles' );

// *********************************
// PERFORMANCE CUSTOMIZATIONS
// *********************************

function remove_head_scripts() { 
	remove_action('wp_head', 'wp_print_scripts'); 
	remove_action('wp_head', 'wp_print_head_scripts', 9); 
	remove_action('wp_head', 'wp_enqueue_scripts', 1);
  
	add_action('wp_footer', 'wp_print_scripts', 5);
	add_action('wp_footer', 'wp_enqueue_scripts', 5);
	add_action('wp_footer', 'wp_print_head_scripts', 5); 
 } 
//  add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );
  
 // END Custom Scripting to Move JavaScript

// *********************************
// Query Changes
// *********************************

function register_country_name() {
	global $wp;
	$wp->add_query_var( 'countryName' );
}
add_action( 'init', 'register_country_name' );


function add_country_name_rewrite_rule()
{
        // add_rewrite_rule(
        // '^country\/([^/]*)$',
        // 'index.php?page_id=3717&countryName=$matches[1]',
		// 'top'
		add_rewrite_tag('%countryName%', '([^&]+)');
		add_rewrite_rule('^country/([a-zA-Z]+( [a-zA-Z]+)?)/?$', 'index.php?pagename=country&countryName=$matches[1]&fwp_country=$matches[1]', 'top');
    
}

add_action('init','add_country_name_rewrite_rule');





/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 * @return string Filtered title.
 */
function wpdocs_filter_wp_title( $title, $sep ) {
    global $paged, $page;
 
    $title = "Apples";
 
    return $title;
}
add_filter( 'wp_title', 'wpdocs_filter_wp_title', 10, 2 );

/**
* Removes width and height attributes from image tags
*
* @param string $html
*
* @return string
*/
function remove_image_size_attributes( $html ) {
	return preg_replace( '/(width|height)="\d*"/', '', $html );
}

// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );

// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );
	



/************************************
/* Gutenburg Functions 
/************************************/
/*
add_filter('use_block_editor_for_post_type', 'prefix_disable_gutenberg');
function prefix_disable_gutenberg($current_status, $post_type)
{
    // Use your post type key instead of 'product'
    if ($post_type === 'incident') return false;
    return $current_status;
}*/

function custom_taxonomies_terms_links() {
    global $post;
    // some custom taxonomies:
    $taxonomies = array( 
						"asd_threat_actor" =>"Threat Actors: ",
                         "asd_tools"=>"Tools: "
                  );
    $out = '<div  class="incident-metabox-tax">';
    foreach ($taxonomies as $tax => $taxname) {     
        $out .= '<div class="incident-metabox-tax-label incident-metabox-tax-label-'. $tax . '">';
        $out .= $taxname;
        // get the terms related to post
        $terms = get_the_terms( $post->ID, $tax );
        if ( !empty( $terms ) ) {
            foreach ( $terms as $term )
                $out .= '<span class="incident-metabox-tax-value '. $tax .'"><a href="' .get_term_link($term->slug, $tax) .'">'.$term->name.'</a></span> ';
        }
        $out .= "</div>";
    }
// 	$fields = get_field_objects();
//     foreach ($fields as $field) {
// 		if($field['value'] != null and is_array($field['value']) != true and $field['name'] == "country"){
// 			$out .= '<div class="incident-metabox-tax-label incident-metabox-tax-label-' . $field['name'] . '">';
//         	$out .= $field['label'];
// 			$out .= ': <span class="incident-metabox-tax-value incident-metabox-tax-value-' . $field['name'] . '">' . $field['value'] . '</span>';
// 			$out .= "</div>";
// 		}
//     }
    $out .= "</div>";
    return $out;
} 

add_shortcode( 'ct_terms', 'custom_taxonomies_terms_links' );

function custom_fields_terms() {

    global $post;
    // some custom taxonomies:
    // 
	$fields = get_field_objects();
	$show_fields = array("country","date_text", "source", "source_2", "source_3", "source_4","source_5");
    $out = '<div class="incident-metabox-fields">';
    foreach ($fields as $field) {     
		if($field['value'] != null and is_array($field['value']) != true and in_array($field['name'], $show_fields)){
			if ($field['name'] == "date_text" or  $field['name'] == "country"  ){
				$out .= '<div class="incident-metabox-field-label incident-metabox-field-label-' . $field['name'] . '">';
			}
			if ($field['name'] == "date_text"){
				$out .= "Date";
			}
			else if( $field['name'] == "country") {
				$out .= $field['label'] . "";
			}
			if ($field['name'] == "date_text" or  $field['name'] == "country"  ){
				$out .= ': <span class="incident-metabox-field-value incident-metabox-field-value-' . $field['name'] . '">' . $field['value'] . '</span>';
			}
			if ($field['name'] == "date_text"  ){
				$out .= "</div>";
			}
		}
    }
	foreach ($fields as $field) {     
		if($field['value'] != null and is_array($field['value']) != true and in_array($field['name'], $show_fields)){
			if ( $field['name'] == "source"   ){
				$out .= '<div class="incident-metabox-field-label incident-metabox-field-label-' . $field['name'] . '">';
				$out .= $field['label'] . ": ";
			}
			
			if($field['name'] == "source" or $field['name'] == "source_2" or $field['name'] == "source_3"  or $field['name'] == "source_4"  or $field['name'] == "source_5"){
                $out .= '<span class="incident-metabox-field-value incident-metabox-field-value-source incident-metabox-field-value-' . $field['name'] . '"><a href="' . $field['value'] .'">'.$field['label'].'</a></span> ';
			}
			if ( $field['name'] == "source_5" ){
				$out .= "</div>";
			}
		}
    }
    $out .= "</div>";
	
	
    return $out;
}

add_filter( 'facetwp_sort_options', function( $options, $params ) {
	unset( $options['date_asc'] );
    unset( $options['date_desc'] );
    unset( $options['title_asc'] );
    unset( $options['title_desc'] );

	
	$options['title_asc'] = array(
        'label' => __( 'Title (A-Z)', 'fwp' ),
        'query_args' => array(
            'orderby' => 'title',
            'order' => 'ASC',
        )
    );
	$options['title_desc'] = array(
        'label' => __( 'Title (Z-A)', 'fwp' ),
        'query_args' => array(
            'orderby' => 'title',
            'order' => 'DESC',
        )
    );
	$options['country_asc'] = array(
        'label' => 'Country (A-Z)',
        'query_args' => array(
            'orderby' => 'country',
            'meta_key' => 'country',
            'order' => 'ASC',
        )
    );
	$options['country_desc'] = array(
        'label' => 'Country (Z-A)',
        'query_args' => array(
            'orderby' => 'country',
            'meta_key' => 'country',
            'order' => 'DESC',
        )
    );
	$options['date_asc'] = array(
        'label' => 'Start Date (Oldest)',
        'query_args' => array(
            'orderby' => 'start_date',
            'meta_key' => 'start_date',
            'order' => 'ASC',
        )
    );
	$options['date_desc'] = array(
        'label' => 'Start Date (Newest)',
        'query_args' => array(
            'orderby' => 'start_date',
            'meta_key' => 'start_date',
            'order' => 'DESC',
        )
    );
    return $options;
}, 10, 2 );


add_shortcode( 'field_terms', 'custom_fields_terms' );

function page_title_sc( ){
   return get_the_title();
}
add_shortcode( 'page_title', 'page_title_sc' );

function page_featured_img( ){
	return wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
}

add_shortcode( 'page_featured_img', 'page_featured_img' );

function page_incident_map($atts) {
    ob_start();
    get_template_part('templates/mapbox');
	get_template_part('templates/threats_hero');
  	return ob_get_clean();
}

add_shortcode( 'page_incident_map', 'page_incident_map' );

add_action( 'wp_footer', 'setFacetIcons' ); // Write our JS below here
function setFacetIcons() { ?>
<script>
(function($) {
    $(document).on('facetwp-loaded', function() {
		console.log('loaded')
        setIcons($);
     });
})(jQuery);
	
jQuery(document).ready(function($) {
	setIcons($);
});

function setIcons($){
	      var base_url = window.location.origin;
    $(".fwpl-term-information-operations").html(function(i, val) {
    	return "<a href='" +base_url + "/asd_tools/information-operations/' title='Information Manipulation'><div class='tool-icon'><i class='glyphicon fa-newspaper fas' aria-hidden='true'></i></div></a>";
	});
     $(".fwpl-term-malign-finance").html(function(i, val) {
    	return "<a href='" +base_url + "/asd_tools/malign-finance/' title='Malign Finance'><div class='tool-icon'><i class='glyphicon fa-funnel-dollar fas' aria-hidden='true'></i></div></a>";
	});
    $(".fwpl-term-cyberattacks").html(function(i, val) {
    	return "<a href='" +base_url + "/asd_tools/cyberattacks/' title='Cyber Operations'><div class='tool-icon'><i class='glyphicon fa-laptop-code fas' aria-hidden='true'></i></div></a>";
	});
    $(".fwpl-term-strategic-economic-coercion").html(function(i, val) {
    	return "<a href='" +base_url + "/asd_tools/strategic-economic-coercion/' title='Economic Coercion'><div class='tool-icon'><i class='glyphicon fa-chart-line fas' aria-hidden='true'></i></div></a>";
	});
    $(".fwpl-term-political-and-social-subversion").html(function(i, val) {
    	return "<a href='" +base_url + "/asd_tools/political-and-social-subversion/' title='Civil Society Subversion'><div class='tool-icon'><i class='glyphicon fa-user-secret fas' aria-hidden='true'></i></div></a>";
     });    
	
}

</script>
<?php
}






// NNIGRO - TRACKER UPDATES


add_filter( 'facetwp_render_output', function( $output ) {
    $output['settings']['threat_actor']['showSearch'] = false;
    $output['settings']['tool']['showSearch'] = false;	
	$output['settings']['country']['showSearch'] = true;
	$output['settings']['regions']['showSearch'] = false;	
	
	
    return $output;
});

// add_filter( 'facetwp_facet_html', function( $output, $params ) {
// 	if ( 'post_type' == $params['facet']['name'] ) {
// 		$current_values = array( 'Malign Finance' );
// 		$replace_values = array( 'HIHIHIHIHIHIHIHIHIHIH', 'For Sale' );
// 		$output = str_replace( $current_values, $replace_values, $output );
// 	}
// 	return $output;
// }, 10, 2 );
// 
?>
