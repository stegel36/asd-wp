<div id="results" class="asd-results">

    <?php while ( have_posts() ): the_post(); ?>
    <div class="asd-result">

        <?php if ( has_post_thumbnail() ) : ?>
            <div class="asd-result__thumb">
                    <div class="asd-result__link">
                        <?php the_post_thumbnail("square-small") ?>
                    </div>
            </div>
        <?php else :  ?>
            <div class="asd-result__thumb"></div>
        <?php endif ?> 
        <?php
        $the_excerpt = get_the_content();
        $the_excerpt = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $the_excerpt);
        $the_excerpt = strip_tags($the_excerpt);
        $the_excerpt = substr($the_excerpt,0,700);
        ?>
        <div class="asd-result__content">   
            <h3 class="asd-result__title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>

            <h4 class="asd-result__date"><?php the_field("date_text");  ?></h4>
            <p class="asd-result__content"><?php echo $the_excerpt; ?></p>
                
        </div>
    </div>

    <?php endwhile; ?>
</div>
